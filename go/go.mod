module TestApp/go

go 1.14

require (
	github.com/go-flutter-desktop/go-flutter v0.35.3
	github.com/go-flutter-desktop/plugins/shared_preferences v0.4.3
	github.com/go-flutter-desktop/plugins/url_launcher v0.1.2
	github.com/pkg/errors v0.9.1
)
