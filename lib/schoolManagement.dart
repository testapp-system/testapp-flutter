import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editExercise.dart';
import 'package:test_app_flutter/style.dart';

import 'assignments.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class SchoolManagement extends StatefulWidget {
  SchoolManagement({
    Key key,
  }) : super(key: key);

  @override
  _SchoolManagementState createState() => _SchoolManagementState();
}

class _SchoolManagementState extends State<SchoolManagement> {
  var api = globals.api;
  List _vouchers = [];
  bool _vouchersLoaded = false;
  List _currentVoucherCodes = [];
  ScrollController _scrollController = ScrollController();

  bool _topicsLoaded = false;
  Map<String, List<Map>> subjectData = {};
  List subjectNames;

  bool savingTopicGrade = false;

  Future<void> listVoucher() async {
    api.call('listVouchers', context: context).then((data) {
      setState(() {
        if (data['response'] is List) _vouchers = data['response'];
        _vouchersLoaded = true;
      });
    });
  }

  Future<void> createVoucher() async {
    api.call('createVoucher', context: context).then((data) {
      setState(() {
        _vouchersLoaded = false;
        _currentVoucherCodes.add(data['response']['code']);
        listVoucher();
      });
    });
  }

  Future<void> listTopics() async {
    globals.api.call('listTopics', context: context).then((data) {
      data['response'].forEach((topic) {
        if (!subjectData.keys.contains(topic['subject']))
          subjectData[topic['subject']] = [];
        subjectData[topic['subject']].add(
            {'name': topic['name'], 'id': topic['id'], 'year': topic['year']});
      });

      setState(() {
        _topicsLoaded = true;
        subjectNames = subjectData.keys.toList();
      });
    });
  }

  @override
  void initState() {
    listVoucher();
    listTopics();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> chips = [];
    _currentVoucherCodes.forEach((code) {
      chips.add(Builder(
        builder: (context) => Chip(
          onDeleted: () {
            Clipboard.setData(new ClipboardData(text: code));
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('Copied voucher!')));
          },
          label: Text(code),
          deleteButtonTooltipMessage: 'Copy',
          deleteIcon: FaIcon(
            FontAwesomeIcons.clone,
            size: 16,
          ),
        ),
      ));
    });

    return ResponsiveDrawerScaffold(
        helpPage: HelpPage(
            path: HelpPagePath.schoolManagement,
            label: S.of(context).helpWithVouchers),
        body: TestAppScrollBar(
          controller: _scrollController,
          child: ListView(
            controller: _scrollController,
            children: [
              TestAppCard(
                children: <Widget>[
                  Text(
                    S.of(context).teacherAccessVoucher,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Text(S
                      .of(context)
                      .forRegistrationTeachersNeedAVoucherVouchersCanBeIssued),
                  ButtonBar(
                    children: <Widget>[
                      ElevatedButton(
                        child: Text(S.of(context).createNewVoucher),
                        onPressed: createVoucher,
                      )
                    ],
                  ),
                  Row(
                    children: chips,
                  ),
                  (_vouchersLoaded)
                      ? (_vouchers.isNotEmpty)
                          ? ListView.separated(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return Builder(
                                    builder: (context) => ListTile(
                                          title: SelectableText(
                                              _vouchers[index]['code']),
                                          subtitle: Text('Tap to copy.'),
                                          trailing: Text(formatFutureDate(
                                              DateTime
                                                  .fromMillisecondsSinceEpoch(
                                                      int.parse(_vouchers[index]
                                                              ['expiry'] +
                                                          '000')))),
                                          onTap: () {
                                            Clipboard.setData(new ClipboardData(
                                                text: _vouchers[index]
                                                    ['code']));
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(SnackBar(
                                                    content: Text(
                                                        'Copied voucher!')));
                                          },
                                        ));
                              },
                              separatorBuilder: (context, index) {
                                return (Divider());
                              },
                              itemCount: _vouchers.length)
                          : ListTile(
                              leading: FaIcon(FontAwesomeIcons.infoCircle),
                              title: Text(S.of(context).thereAreNoVouchersYet),
                            )
                      : CenterProgress()
                ],
              ),
              TestAppCard(
                children: [
                  Text(
                    S.of(context).schoolwideTopicGradeOverwrites,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Text(S
                      .of(context)
                      .asCurriculumVarriesEvenWithinOneStateEverySchoolCan),
                  (_topicsLoaded)
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (c, i) => AwesomeExpansionTile(
                                  title: Text(subjectNames[i]),
                                  children: [
                                    ListView.separated(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount:
                                          subjectData[subjectNames[i]].length,
                                      itemBuilder: (c, index) {
                                        Map currentTopic =
                                            subjectData[subjectNames[i]][index];
                                        return AwesomeExpansionTile(
                                          title: Text(currentTopic['name']),
                                          leading: Text(
                                              currentTopic['year'].toString()),
                                          children: [
                                            Divider(),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      56, 8, 16, 8),
                                              child: GradeSlider(
                                                initialValue: int.parse(
                                                    currentTopic['year']),
                                                onChanged: (newGrade) async {
                                                  if (newGrade !=
                                                      currentTopic['year']) {
                                                    setState(() {
                                                      savingTopicGrade = true;
                                                    });
                                                    api
                                                        .call(
                                                            'topicYearOverwrite',
                                                            options: {
                                                              'topic':
                                                                  currentTopic[
                                                                      'id'],
                                                              'year': newGrade
                                                            },
                                                            context: context)
                                                        .then((value) =>
                                                            setState(() =>
                                                                savingTopicGrade =
                                                                    false));
                                                  }
                                                },
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                      separatorBuilder: (c, i) => Divider(),
                                    ),
                                  ]),
                          separatorBuilder: (c, i) => Divider(),
                          itemCount: subjectNames.length)
                      : CenterProgress()
                ],
              )
            ],
          ),
        ),
        floatingActionButton: (savingTopicGrade)
            ? FloatingActionButton(
                onPressed: () {},
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white)))
            : null);
  }
}
