import 'dart:async';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:katex_flutter/katex_flutter.dart';
import 'package:test_app_flutter/assignments.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/testScore.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class WriteTest extends StatefulWidget {
  final List exercises;
  final int joinId;
  final Map assignment;
  final int testTime;

  WriteTest(
      {Key key,
      @required this.exercises,
      this.joinId,
      this.assignment,
      this.testTime})
      : super(key: key);

  @override
  _WriteTestState createState() => _WriteTestState();
}

class _WriteTestState extends State<WriteTest> {
  var api = globals.api;
  Map<String, List> _selectedAnswers = Map();
  List<Widget> exerciseCards = [];

  bool _showProgress = false;

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    var exercises = widget.exercises;

    if (widget.assignment != null) {
      // Saving preselected exercises
      bool errorParsingAnswers = false;
      try {
        widget.assignment['answers'].forEach(
            (a) => _selectedAnswers[a['exercise'].toString()] = a['answer']);
      } catch (e) {
        errorParsingAnswers = true;
      }
      exerciseCards.add(Builder(
        builder: (context) => TestAppCard(
          children: [
            Row(
              children: [
                Text(
                  widget.assignment['class'] + ': ',
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                Text(
                  widget.assignment['name'],
                  style: Theme.of(context).textTheme.headline5,
                ),
                if (errorParsingAnswers)
                  Text(
                    'We unfortunately couldn\'t read your previous answers.',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontStyle: FontStyle.italic),
                  )
              ],
            ),
            Text(widget.assignment['describtion'])
          ],
        ),
      ));
    }
    // Building exercise boxes
    for (int i = 0; i < exercises.length; i++) {
      exerciseCards.add(ExerciseBox(
          data: exercises[i],
          selectedAnswers: _selectedAnswers[exercises[i]['id']],
          onChange: (answers) {
            _selectedAnswers[exercises[i]['id']] = answers;
          }));
    }
    // Timeout for joinTests
    if (widget.testTime != null)
      Timer(Duration(seconds: widget.testTime), submitTest);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onPopPressed,
      child: ResponsiveDrawerScaffold(
          helpPage: HelpPage(
              path: HelpPagePath.writeTest,
              label: S.of(context).helpWritingTest),
          appBarActions: (widget.testTime != null)
              ? <Widget>[
                  JoinTestTimer(
                    time: widget.testTime,
                    color: Colors.white,
                  )
                ]
              : null,
          body: (!_showProgress)
              ? TestAppScrollBar(
                  controller: _scrollController,
                  child: SingleChildScrollView(
                    controller: _scrollController,
                    child: Flex(
                      direction: Axis.vertical,
                      //mainAxisSize: MainAxisSize.max,
                      children: List.from(exerciseCards)
                        ..addAll([
                          TestAppCard(
                            children: <Widget>[
                              Text(
                                (widget.assignment != null)
                                    ? S.of(context).submitAssignment
                                    : S.of(context).saveTest,
                                style: Theme.of(context).textTheme.headline6,
                              ),
                              Wrap(
                                  spacing: 8,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  children: [
                                    ElevatedButton.icon(
                                      icon: FaIcon(FontAwesomeIcons.paperPlane),
                                      label: Text(S.of(context).submitTest),
                                      onPressed: submitTest,
                                    ),
                                    Text(S.of(context).goodLuck)
                                  ])
                            ],
                          )
                        ]),
                    ),
                  ),
                )
              : CenterProgress(
                  label: S.of(context).savingYourTest,
                )),
    );
  }

  Future<void> submitTest() async {
    setState(() {
      _showProgress = true;
    });
    Map<String, dynamic> options = Map();
    options['answers'] = _selectedAnswers;
    if (widget.joinId != null) options['join'] = widget.joinId;
    if (widget.assignment != null)
      options['assignment'] = widget.assignment['id'];
    api.call("saveTest", options: options, context: context).then((response) {
      var testId = response['response'];
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => (widget.assignment != null)
                ? Assignments()
                : TestScore(
                    testId: testId,
                    backToUserScore: true,
                  )),
      );
    });
  }

  Future<bool> _onPopPressed() {
    return showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: Text((widget.joinId == null)
                      ? S.of(context).doYouRealyWantToAbortThisTestYourAnswers
                      : S.of(context).doYouRealyWantToAbortThisJoinTestIt),
                  actions: <Widget>[
                    MaterialButton(
                      onPressed: () {
                        if (widget.joinId != null) submitTest();
                        return Navigator.of(context)
                            .pop((widget.joinId == null));
                      },
                      child: Text(S.of(context).abortTest),
                    ),
                    MaterialButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text(S.of(context).continueWriting),
                    )
                  ],
                )) ??
        false;
  }
}

class JoinTestTimer extends StatefulWidget {
  final int time;
  final Color color;

  JoinTestTimer({Key key, @required this.time, this.color = Colors.white})
      : super(key: key);

  @override
  _JoinTestTimerState createState() => _JoinTestTimerState();
}

class _JoinTestTimerState extends State<JoinTestTimer> {
  int _timeLeft = -1;
  Timer _timer;

  String _printDuration(Duration duration) {
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  @override
  void initState() {
    _timer = Timer.periodic(new Duration(seconds: 1), (timer) {
      if ((widget.time - timer.tick) <= 0) {
        setState(() {
          _timeLeft = 0;
        });
      } else
        setState(() {
          _timeLeft = widget.time - timer.tick;
        });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_timeLeft == -1) _timeLeft = widget.time;

    return Center(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        _printDuration(Duration(seconds: _timeLeft)),
        style:
            Theme.of(context).textTheme.headline6.copyWith(color: widget.color),
      ),
    ));
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}

class ExerciseBox extends StatefulWidget {
  final Map data;
  final List selectedAnswers; //For initialization only
  final Function(List answers) onChange;

  ExerciseBox(
      {@required this.data, @required this.onChange, this.selectedAnswers});

  @override
  _ExerciseBoxState createState() => _ExerciseBoxState();
}

class _ExerciseBoxState extends State<ExerciseBox>
    with SingleTickerProviderStateMixin {
  String title;
  List answers = [];
  Widget question;
  Widget image;
  List<Widget> texViews = [];
  List _selectedAnswers;
  List<TextEditingController> _inputControllers = [];
  bool _rendered = false;

  @override
  void initState() {
    _createContent();
    super.initState();
  }

  void _createContent() async {
    title = widget.data['name'];
    question = TestAppTex(widget.data['content']);
    if (widget.selectedAnswers != null) {
      _selectedAnswers = widget.selectedAnswers;
    } else {
      _selectedAnswers = [];
    }

    for (int i = 1; i < 6; i++) {
      if (widget.data["aw$i"] != "") answers.add(widget.data["aw$i"]);
    }

    if (widget.data['img'] != null && widget.data['img'] != "") {
      final UriData data = Uri.parse(widget.data['img']).data;
      image = Container(
          child: Image.memory(
        data.contentAsBytes(),
        scale: 2,
      ));
    } else
      image = Container();
    _createAnswersList();
  }

  void _createAnswersList() {
    switch (widget.data['awtype']) {
      case "radio":
      case "checkbox":
        {
          texViews = List.generate(answers.length, (index) {
            return TestAppTex(answers[index]);
          });
        }
        break;
      case "regex":
      case "text":
        {
          _inputControllers.add(TextEditingController(
              text: (_selectedAnswers.isNotEmpty) ? _selectedAnswers[0] : ''));
        }
        break;
      case "number":
        {
          _inputControllers.add(TextEditingController(
              text: (_selectedAnswers.isNotEmpty) ? _selectedAnswers[0] : ''));
        }
        break;
      case "fillInText":
      case "fillInRegEx":
      case "inputMultiple":
      case "multipleRegEx":
        {
          _inputControllers = List.generate(answers.length, (n) {
            return (TextEditingController(
                text:
                    (_selectedAnswers.length > n) ? _selectedAnswers[n] : ''));
          });
        }
        break;
    }
    setState(() {
      _rendered = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!_rendered)
      return (TestAppCard(
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context).textTheme.headline6,
          ),
          CenterProgress()
        ],
      ));
    Widget answerContainer = Text(widget.data['awtype']);

    switch (widget.data['awtype']) {
      case "checkbox":
        {
          answerContainer = ListView.separated(
            shrinkWrap: true,
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            itemCount: answers.length,
            itemBuilder: (context, int n) {
              return (CheckboxListTile(
                value: _selectedAnswers.contains(answers[n]),
                title: texViews[n],
                onChanged: (bool selected) {
                  setState(() {
                    (selected)
                        ? _selectedAnswers.add(answers[n])
                        : _selectedAnswers.remove(answers[n]);
                  });
                  save(null);
                },
              ));
            },
            separatorBuilder: (c, i) => Divider(),
          );
        }
        break;
      case "radio":
        {
          answerContainer = ListView.separated(
            shrinkWrap: true,
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            itemCount: answers.length,
            itemBuilder: (context, int n) {
              return (RadioListTile(
                  value: answers[n],
                  groupValue:
                      (_selectedAnswers.isEmpty) ? null : _selectedAnswers[0],
                  title: texViews[n],
                  onChanged: (selected) {
                    setState(() {
                      _selectedAnswers = [selected];
                    });
                    save(null);
                  }));
            },
            separatorBuilder: (c, i) => Divider(),
          );
        }
        break;
      case "regex":
      case "text":
        {
          answerContainer = TextField(
            controller: _inputControllers[0],
            onChanged: save,
            decoration: InputDecoration(
                border: UnderlineInputBorder(),
                labelText: S.of(context).answer,
                helperText: S.of(context).lettersNumbersAndTextAllowed),
          );
        }
        break;
      case "number":
        {
          answerContainer = TextField(
            controller: _inputControllers[0],
            onChanged: save,
            keyboardType:
                TextInputType.numberWithOptions(decimal: true, signed: true),
            decoration: InputDecoration(
                border: UnderlineInputBorder(),
                labelText: S.of(context).answer,
                helperText: S.of(context).numbersOnly),
          );
        }
        break;
      case "fillInText":
      case "fillInRegEx":
      case "inputMultiple":
      case "multipleRegEx":
        {
          answerContainer = ListView.builder(
              shrinkWrap: true,
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              itemCount: answers.length + ((answers.length == 1) ? 0 : 1),
              itemBuilder: (context, int n) {
                if (n == 0 && answers.length != 1)
                  return (Text(
                    (['fillInText', 'fillInRegEx']
                            .contains(widget.data['awtype']))
                        ? S.of(context).orderMatters
                        : S.of(context).orderDoesntMatter,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ));
                return (ListTile(
                  title: TextField(
                    controller:
                        _inputControllers[n - ((answers.length == 1) ? 0 : 1)],
                    onChanged: save,
                    decoration: InputDecoration(
                        labelText: S.of(context).answer +
                            ' ' +
                            ((answers.length == 1) ? '' : n.toString())),
                  ),
                ));
              });
        }
        break;
    }

    return (TestAppCard(
      children: <Widget>[
        Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
        image,
        question,
        answerContainer
        //answerContainer
      ],
    ));
  }

  void save(changed) {
    if (_inputControllers.isNotEmpty) {
      _selectedAnswers.clear();
      _inputControllers.forEach((controller) {
        _selectedAnswers.add(controller.text);
      });
    }

    _selectedAnswers.forEach((el) => {if (el is String) el.trim()});

    if (!['fillInText', 'fillInRegEx'].contains(widget.data['awtype']))
      _selectedAnswers.sort();

    widget.onChange(_selectedAnswers);
    setState(() {});
  }
}

class TestAppTex extends StatefulWidget {
  final String texCode;
  final bool inheritWidth;
  final Color background;

  TestAppTex(this.texCode, {this.inheritWidth = true, this.background});

  @override
  _TestAppTexState createState() => _TestAppTexState();
}

class _TestAppTexState extends State<TestAppTex> with WidgetsBindingObserver {
  @override
  void didChangePlatformBrightness() {
    print(WidgetsBinding.instance.window
        .platformBrightness); // should print Brightness.light / Brightness.dark when you switch
    super.didChangePlatformBrightness(); // make sure you call this
  }

  @override
  Widget build(BuildContext context) {
    return (widget.texCode.contains('\$'))
        ? KaTeX(
            laTeXCode: Text(widget.texCode.replaceAll('\n', '<br/>')),
            inheritWidth: widget.inheritWidth,
            background: (widget.background == null)
                ? Theme.of(context).cardColor
                : widget.background,
          )
        : Text(widget.texCode);
  }
}

String twoDigits(int n) {
  if (n > 9) return "$n";
  return "0$n";
}
