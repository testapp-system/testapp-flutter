import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';
import 'package:test_app_flutter/api.dart';
import 'package:test_app_flutter/chart.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/practise.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/testScore.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class UserScore extends StatefulWidget {
  UserScore({
    Key key,
  }) : super(key: key);

  @override
  _UserScoreState createState() => _UserScoreState();
}

class _UserScoreState extends State<UserScore> {
  int _fetched = 0;
  double _userScore = -1;
  List _tests = [];
  Map _subjectScores = Map();

  bool _noScoreAvailable = false;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    globals.api.call("userScore", context: context).then((response) {
      setState(() {
        _fetched++;
        if (!(response['response'] is bool)) {
          _userScore = response['response'];
        } else
          _noScoreAvailable = true;
      });
    });
    globals.api.call("listTests", context: context).then((response) {
      setState(() {
        _fetched++;
        if (!(response['response'] is bool)) {
          _tests = response['response'];
        } else
          _tests = [];
      });
    });
    globals.api.call("listTopics", context: context).then((response) {
      setState(() {
        _fetched++;
      });
      _subjectScores.clear();
      Set subjects = Set();
      response['response'].forEach((topic) => subjects.add(topic['subject']));
      subjects.forEach((subject) async => globals.api
              .call("userSubjectScore",
                  options: {'subject': subject}, context: context)
              .then((response) {
            if (!(response['response'] is bool)) {
              setState(() {
                _subjectScores[subject] = (response['response'] * 100).round();
              });
              Preferences().save('knownSubjectNames',
                  jsonEncode(_subjectScores.keys.toList()));
            }
          }));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
        helpPage: HelpPage(
            path: HelpPagePath.yourScore,
            label: S.of(context).helpWithYourScore),
        body: (_fetched >= 3)
            ? (!_noScoreAvailable)
                ? TestAppScrollBar(
                    controller: _scrollController,
                    child: ListView.separated(
                        controller: _scrollController,
                        separatorBuilder: (BuildContext context, int index) =>
                            (index != 0) ? const Divider() : Container(),
                        itemCount: _tests.length + 2,
                        itemBuilder: (context, index) {
                          if (index == 0)
                            return TestAppCard(
                                children: [ScoreChart(100 * _userScore)]);
                          if (index == 1) {
                            List<Widget> subjectCards = [];
                            _subjectScores.keys.toList().forEach((subjectName) {
                              var background = colorByText(subjectName);
                              subjectCards.add(Card(
                                color: background,
                                child: SizedBox(
                                  width: 200,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        subjectName,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6
                                            .copyWith(color: Colors.white),
                                      ),
                                      Text(
                                        _subjectScores[subjectName].toString() +
                                            "%",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline2
                                            .copyWith(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                      )
                                    ],
                                  ),
                                ),
                              ));
                            });
                            return SizedBox(
                                height: 150,
                                child: (ListView(
                                    scrollDirection: Axis.horizontal,
                                    children: subjectCards)));
                          }
                          var currentTest = _tests.reversed.toList()[index - 2];
                          return (TestScoreDetailTile(
                            testId: currentTest['id'],
                            timestamp: currentTest['timestamp'],
                          ));
                        }),
                  )
                : Column(
                    children: <Widget>[
                      TestAppCard(
                        children: [
                          Text(
                            S.of(context).noScoreAvailable,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          ListTile(
                              leading: FaIcon(FontAwesomeIcons.infoCircle),
                              title:
                                  Text(S.of(context).youDidntWriteAnyTestYet),
                              trailing: OutlinedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (b) => Practise()));
                                },
                                child: Text(S.of(context).writeTest),
                              ))
                        ],
                      ),
                    ],
                  )
            : CenterProgress()));
  }
}

class TestScoreDetailTile extends StatefulWidget {
  final String testId;
  final String timestamp; //currentTest['timestamp']

  TestScoreDetailTile({@required this.testId, @required this.timestamp});

  @override
  _TestScoreDetailTileState createState() => _TestScoreDetailTileState();
}

class _TestScoreDetailTileState extends State<TestScoreDetailTile> {
  bool _detailsLoaded = false;
  double _testScore;
  DateTime _time;

  void load() async {
    Map<String, dynamic> options = Map();
    options['id'] = widget.testId;
    globals.api
        .call("testScore", options: options, context: context)
        .then((response) {
      if (!(response['response'] is bool)) {
        _testScore = response['response']['score'].toDouble();
      } else {
        _testScore = -1.0;
      }
      setState(() {
        _detailsLoaded = true;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _time = new DateTime.fromMillisecondsSinceEpoch(
        int.parse(widget.timestamp + '000'));
  }

  @override
  Widget build(BuildContext context) {
    return (AwesomeExpansionTile(
      onExpansionChanged: (opened) {
        if (opened && !_detailsLoaded) load();
      },
      leading: Text(widget.testId),
      title: Text(timeago.format(_time)),
      children: <Widget>[
        (_detailsLoaded)
            ? TestScoreDetail(
                score: _testScore,
                testId: widget.testId,
                timestamp: _time,
              )
            : CenterProgress()
      ],
    ));
  }
}

class TestScoreDetail extends StatelessWidget {
  final double score;
  final String testId;
  final DateTime timestamp;

  TestScoreDetail(
      {@required this.score, @required this.testId, @required this.timestamp});

  @override
  Widget build(BuildContext context) {
    return TestAppCard(children: [
      ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          ListTile(
            leading: FaIcon(FontAwesomeIcons.hashtag),
            title: Text(testId.toString()),
          ),
          (score != -1.0)
              ? ScoreChart(score * 100)
              : Center(child: Text(S.of(context).noScoreAvailable)),
          ListTile(
            leading: FaIcon(FontAwesomeIcons.info),
            title: ElevatedButton(
              child: Text(S.of(context).moreDetails),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TestScore(
                            testId: int.parse(testId),
                          )),
                );
              },
            ),
          ),
          ListTile(
            leading: FaIcon(FontAwesomeIcons.shareAlt),
            title: OutlinedButton(
                onPressed: () {
                  Share.share(S.of(context).myTestScoreOnTestappFrom +
                      """+${timeago.format(timestamp)}:

${(score * 100).round()}%""");
                },
                child: Text(S.of(context).shareThisScore)),
          ),
        ],
      )
    ]);
  }
}
