// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Learning. Easily.`
  String get slogan {
    return Intl.message(
      'Learning. Easily.',
      name: 'slogan',
      desc: '',
      args: [],
    );
  }

  /// `Random Test`
  String get randomTest {
    return Intl.message(
      'Random Test',
      name: 'randomTest',
      desc: '',
      args: [],
    );
  }

  /// `Join Test`
  String get joinTest {
    return Intl.message(
      'Join Test',
      name: 'joinTest',
      desc: '',
      args: [],
    );
  }

  /// `Your Score`
  String get yourScore {
    return Intl.message(
      'Your Score',
      name: 'yourScore',
      desc: '',
      args: [],
    );
  }

  /// `Exercises`
  String get exercises {
    return Intl.message(
      'Exercises',
      name: 'exercises',
      desc: '',
      args: [],
    );
  }

  /// `Classes`
  String get classes {
    return Intl.message(
      'Classes',
      name: 'classes',
      desc: '',
      args: [],
    );
  }

  /// `Exercise Sets`
  String get joinTests {
    return Intl.message(
      'Exercise Sets',
      name: 'joinTests',
      desc: '',
      args: [],
    );
  }

  /// `School management`
  String get schoolManagement {
    return Intl.message(
      'School management',
      name: 'schoolManagement',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get about {
    return Intl.message(
      'About',
      name: 'about',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Save Test`
  String get saveTest {
    return Intl.message(
      'Save Test',
      name: 'saveTest',
      desc: '',
      args: [],
    );
  }

  /// `Submit Test`
  String get submitTest {
    return Intl.message(
      'Submit Test',
      name: 'submitTest',
      desc: '',
      args: [],
    );
  }

  /// `Good luck!`
  String get goodLuck {
    return Intl.message(
      'Good luck!',
      name: 'goodLuck',
      desc: '',
      args: [],
    );
  }

  /// `Saving your test...`
  String get savingYourTest {
    return Intl.message(
      'Saving your test...',
      name: 'savingYourTest',
      desc: '',
      args: [],
    );
  }

  /// `Do you really want to abort this test? Your answers will be lost.`
  String get doYouRealyWantToAbortThisTestYourAnswers {
    return Intl.message(
      'Do you really want to abort this test? Your answers will be lost.',
      name: 'doYouRealyWantToAbortThisTestYourAnswers',
      desc: '',
      args: [],
    );
  }

  /// `Do you really want to abort this Join Test? It will be automatically submitted with your current answers.`
  String get doYouRealyWantToAbortThisJoinTestIt {
    return Intl.message(
      'Do you really want to abort this Join Test? It will be automatically submitted with your current answers.',
      name: 'doYouRealyWantToAbortThisJoinTestIt',
      desc: '',
      args: [],
    );
  }

  /// `Abort Test`
  String get abortTest {
    return Intl.message(
      'Abort Test',
      name: 'abortTest',
      desc: '',
      args: [],
    );
  }

  /// `Continue writing`
  String get continueWriting {
    return Intl.message(
      'Continue writing',
      name: 'continueWriting',
      desc: '',
      args: [],
    );
  }

  /// `Answer`
  String get answer {
    return Intl.message(
      'Answer',
      name: 'answer',
      desc: '',
      args: [],
    );
  }

  /// `Letters, numbers and text allowed`
  String get lettersNumbersAndTextAllowed {
    return Intl.message(
      'Letters, numbers and text allowed',
      name: 'lettersNumbersAndTextAllowed',
      desc: '',
      args: [],
    );
  }

  /// `Numbers only`
  String get numbersOnly {
    return Intl.message(
      'Numbers only',
      name: 'numbersOnly',
      desc: '',
      args: [],
    );
  }

  /// `Order matters.`
  String get orderMatters {
    return Intl.message(
      'Order matters.',
      name: 'orderMatters',
      desc: '',
      args: [],
    );
  }

  /// `Order doesn't matter.`
  String get orderDoesntMatter {
    return Intl.message(
      'Order doesn\'t matter.',
      name: 'orderDoesntMatter',
      desc: '',
      args: [],
    );
  }

  /// `No score available`
  String get noScoreAvailable {
    return Intl.message(
      'No score available',
      name: 'noScoreAvailable',
      desc: '',
      args: [],
    );
  }

  /// `You didn't write any test yet.`
  String get youDidntWriteAnyTestYet {
    return Intl.message(
      'You didn\'t write any test yet.',
      name: 'youDidntWriteAnyTestYet',
      desc: '',
      args: [],
    );
  }

  /// `Write test`
  String get writeTest {
    return Intl.message(
      'Write test',
      name: 'writeTest',
      desc: '',
      args: [],
    );
  }

  /// `More details`
  String get moreDetails {
    return Intl.message(
      'More details',
      name: 'moreDetails',
      desc: '',
      args: [],
    );
  }

  /// `Share this Score`
  String get shareThisScore {
    return Intl.message(
      'Share this Score',
      name: 'shareThisScore',
      desc: '',
      args: [],
    );
  }

  /// `My test Score on TestApp from`
  String get myTestScoreOnTestappFrom {
    return Intl.message(
      'My test Score on TestApp from',
      name: 'myTestScoreOnTestappFrom',
      desc: '',
      args: [],
    );
  }

  /// `Test score`
  String get testScore {
    return Intl.message(
      'Test score',
      name: 'testScore',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Student's`
  String get studentGenitive {
    return Intl.message(
      'Student\'s',
      name: 'studentGenitive',
      desc: '',
      args: [],
    );
  }

  /// `Your`
  String get your {
    return Intl.message(
      'Your',
      name: 'your',
      desc: '',
      args: [],
    );
  }

  /// `Student's answer:`
  String get studentsAnswer {
    return Intl.message(
      'Student\'s answer:',
      name: 'studentsAnswer',
      desc: '',
      args: [],
    );
  }

  /// `Correct answer:`
  String get correctAnswer {
    return Intl.message(
      'Correct answer:',
      name: 'correctAnswer',
      desc: '',
      args: [],
    );
  }

  /// `Your answer:`
  String get yourAnswer {
    return Intl.message(
      'Your answer:',
      name: 'yourAnswer',
      desc: '',
      args: [],
    );
  }

  /// `No data available`
  String get noDataAvailable {
    return Intl.message(
      'No data available',
      name: 'noDataAvailable',
      desc: '',
      args: [],
    );
  }

  /// `Error fetching your score. Maybe the test submission was broken.`
  String get errorFetchingYourScoreMaybeTheTestSumbissionWasBroken {
    return Intl.message(
      'Error fetching your score. Maybe the test submission was broken.',
      name: 'errorFetchingYourScoreMaybeTheTestSumbissionWasBroken',
      desc: '',
      args: [],
    );
  }

  /// `Go back`
  String get goBack {
    return Intl.message(
      'Go back',
      name: 'goBack',
      desc: '',
      args: [],
    );
  }

  /// `Loading data...`
  String get loadingData {
    return Intl.message(
      'Loading data...',
      name: 'loadingData',
      desc: '',
      args: [],
    );
  }

  /// `Account`
  String get account {
    return Intl.message(
      'Account',
      name: 'account',
      desc: '',
      args: [],
    );
  }

  /// `Log out`
  String get logOut {
    return Intl.message(
      'Log out',
      name: 'logOut',
      desc: '',
      args: [],
    );
  }

  /// `Update password`
  String get updatePassword {
    return Intl.message(
      'Update password',
      name: 'updatePassword',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a new password. We recommend choosing a password containing letters, symbols and numbers with at least 8 charcters.`
  String get pleaseProvideANewPasswordWeRecommendChoosingAPassword {
    return Intl.message(
      'Please provide a new password. We recommend choosing a password containing letters, symbols and numbers with at least 8 charcters.',
      name: 'pleaseProvideANewPasswordWeRecommendChoosingAPassword',
      desc: '',
      args: [],
    );
  }

  /// `New password`
  String get newPassword {
    return Intl.message(
      'New password',
      name: 'newPassword',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Teacher access voucher`
  String get teacherAccessVoucher {
    return Intl.message(
      'Teacher access voucher',
      name: 'teacherAccessVoucher',
      desc: '',
      args: [],
    );
  }

  /// `For registration, teachers need a voucher. Vouchers can be issued by any other teacher and need to be entered during the registration process. Vouchers are valid for one week after their creation.`
  String get forRegistrationTeachersNeedAVoucherVouchersCanBeIssued {
    return Intl.message(
      'For registration, teachers need a voucher. Vouchers can be issued by any other teacher and need to be entered during the registration process. Vouchers are valid for one week after their creation.',
      name: 'forRegistrationTeachersNeedAVoucherVouchersCanBeIssued',
      desc: '',
      args: [],
    );
  }

  /// `Create new voucher`
  String get createNewVoucher {
    return Intl.message(
      'Create new voucher',
      name: 'createNewVoucher',
      desc: '',
      args: [],
    );
  }

  /// `There are no vouchers yet.`
  String get thereAreNoVouchersYet {
    return Intl.message(
      'There are no vouchers yet.',
      name: 'thereAreNoVouchersYet',
      desc: '',
      args: [],
    );
  }

  /// `Short Test (5 exercises)`
  String get shortTest5Exercises {
    return Intl.message(
      'Short Test (5 exercises)',
      name: 'shortTest5Exercises',
      desc: '',
      args: [],
    );
  }

  /// `Smart Test (10 exercises)`
  String get smartTest10Exercises {
    return Intl.message(
      'Smart Test (10 exercises)',
      name: 'smartTest10Exercises',
      desc: '',
      args: [],
    );
  }

  /// `Please select`
  String get pleaseSelect {
    return Intl.message(
      'Please select',
      name: 'pleaseSelect',
      desc: '',
      args: [],
    );
  }

  /// `Start test`
  String get startTest {
    return Intl.message(
      'Start test',
      name: 'startTest',
      desc: '',
      args: [],
    );
  }

  /// `Next`
  String get next {
    return Intl.message(
      'Next',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `Loading test...`
  String get loadingTest {
    return Intl.message(
      'Loading test...',
      name: 'loadingTest',
      desc: '',
      args: [],
    );
  }

  /// `Topics`
  String get topics {
    return Intl.message(
      'Topics',
      name: 'topics',
      desc: '',
      args: [],
    );
  }

  /// `Test Type`
  String get testType {
    return Intl.message(
      'Test Type',
      name: 'testType',
      desc: '',
      args: [],
    );
  }

  /// `Datenschutz ist uns wichtig`
  String get privacyIsImportantToUs {
    return Intl.message(
      'Datenschutz ist uns wichtig',
      name: 'privacyIsImportantToUs',
      desc: '',
      args: [],
    );
  }

  /// `Dein Score aus Tests ist nur für dich und die Leiter der jeweiligen Kurse sichtbar. Bei jedem Test kannst du ebenfalls auswählen, dass dieser gänzlich privat bleibt. Die ausführliche Beschreibung, wie wir deine Daten behandeln findest du in unserer Datenschutzerklärung.  Es kann dich niemand, weder Eltern oder Freunde, noch Lehrer, zwingen, unsere TestApp und zu benutzen und damit unserer Datenschutzerklärung zuzustimmen.  Falls du Bedenken bezüglich unseres Datenschutzes hast, wende dich gerne an info@testapp.ga. Wenn du dich irgendwann entscheidest, dass du mit unserer Datenverarbeitung nicht mehr einverstanden bist, kannst du jederzeit dein Einverständnis zurückziehen. Weil deine Daten schließlich dir gehoren, kannst du sie auch jederzeit löschen, korrigieren und einsehen. Wir möchten dir den Datenschutz nicht schwer machen und dir deshalb keine komplizierten Formulare vorsetzen. Wenn du von deinen Rechten gebrauch machen willst, kannst du dich deshalb ganz einfach in einer unformalen E-Mail an info@testapp.ga wenden.  Falls du unter 16 Jahre alt bist, bist du dazu verpflichtet, deine Eltern oder Sorgeberechtigen über deine Anmeldung zu informieren und diese müssen unseren Datenschutz-Richtlinien zustimmen.`
  String get privacyPolicyShort {
    return Intl.message(
      'Dein Score aus Tests ist nur für dich und die Leiter der jeweiligen Kurse sichtbar. Bei jedem Test kannst du ebenfalls auswählen, dass dieser gänzlich privat bleibt. Die ausführliche Beschreibung, wie wir deine Daten behandeln findest du in unserer Datenschutzerklärung.  Es kann dich niemand, weder Eltern oder Freunde, noch Lehrer, zwingen, unsere TestApp und zu benutzen und damit unserer Datenschutzerklärung zuzustimmen.  Falls du Bedenken bezüglich unseres Datenschutzes hast, wende dich gerne an info@testapp.ga. Wenn du dich irgendwann entscheidest, dass du mit unserer Datenverarbeitung nicht mehr einverstanden bist, kannst du jederzeit dein Einverständnis zurückziehen. Weil deine Daten schließlich dir gehoren, kannst du sie auch jederzeit löschen, korrigieren und einsehen. Wir möchten dir den Datenschutz nicht schwer machen und dir deshalb keine komplizierten Formulare vorsetzen. Wenn du von deinen Rechten gebrauch machen willst, kannst du dich deshalb ganz einfach in einer unformalen E-Mail an info@testapp.ga wenden.  Falls du unter 16 Jahre alt bist, bist du dazu verpflichtet, deine Eltern oder Sorgeberechtigen über deine Anmeldung zu informieren und diese müssen unseren Datenschutz-Richtlinien zustimmen.',
      name: 'privacyPolicyShort',
      desc: '',
      args: [],
    );
  }

  /// `I read the full privacy statement and agree to all mentioned points`
  String get iReadTheFullPrivacyStatementAndAgreeToAll {
    return Intl.message(
      'I read the full privacy statement and agree to all mentioned points',
      name: 'iReadTheFullPrivacyStatementAndAgreeToAll',
      desc: '',
      args: [],
    );
  }

  /// `Reset password`
  String get resetPassword {
    return Intl.message(
      'Reset password',
      name: 'resetPassword',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to reset the password for`
  String get areYouSureToResetThePasswordFor {
    return Intl.message(
      'Are you sure to reset the password for',
      name: 'areYouSureToResetThePasswordFor',
      desc: '',
      args: [],
    );
  }

  /// `Reset`
  String get reset {
    return Intl.message(
      'Reset',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  /// `Please check your mails.`
  String get pleaseCheckYourMails {
    return Intl.message(
      'Please check your mails.',
      name: 'pleaseCheckYourMails',
      desc: '',
      args: [],
    );
  }

  /// `School: `
  String get school {
    return Intl.message(
      'School: ',
      name: 'school',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Mail ID`
  String get mailId {
    return Intl.message(
      'Mail ID',
      name: 'mailId',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Wrong mail id or password.`
  String get wrongMailIdOrPassword {
    return Intl.message(
      'Wrong mail id or password.',
      name: 'wrongMailIdOrPassword',
      desc: '',
      args: [],
    );
  }

  /// `Stay logged in:`
  String get stayLoggedIn {
    return Intl.message(
      'Stay logged in:',
      name: 'stayLoggedIn',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get register {
    return Intl.message(
      'Register',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `This is important for your teachers.`
  String get thisIsImportantForYourTeachers {
    return Intl.message(
      'This is important for your teachers.',
      name: 'thisIsImportantForYourTeachers',
      desc: '',
      args: [],
    );
  }

  /// `Full name`
  String get fullName {
    return Intl.message(
      'Full name',
      name: 'fullName',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a full name, a valid mail id and a password.`
  String get pleaseProvideAFullNameAValidMailIdAnd {
    return Intl.message(
      'Please provide a full name, a valid mail id and a password.',
      name: 'pleaseProvideAFullNameAValidMailIdAnd',
      desc: '',
      args: [],
    );
  }

  /// `Teacher Register`
  String get teacherRegister {
    return Intl.message(
      'Teacher Register',
      name: 'teacherRegister',
      desc: '',
      args: [],
    );
  }

  /// `This is important for your students.`
  String get thisIsImportantForYourStudents {
    return Intl.message(
      'This is important for your students.',
      name: 'thisIsImportantForYourStudents',
      desc: '',
      args: [],
    );
  }

  /// `Access voucher`
  String get accessVoucher {
    return Intl.message(
      'Access voucher',
      name: 'accessVoucher',
      desc: '',
      args: [],
    );
  }

  /// `If you didn't get a voucher, please contact your school's admin.`
  String get ifYouDidntGetAVoucherPleaseContactYourSchools {
    return Intl.message(
      'If you didn\'t get a voucher, please contact your school\'s admin.',
      name: 'ifYouDidntGetAVoucherPleaseContactYourSchools',
      desc: '',
      args: [],
    );
  }

  /// `Connect to other server...`
  String get connectToOtherServer {
    return Intl.message(
      'Connect to other server...',
      name: 'connectToOtherServer',
      desc: '',
      args: [],
    );
  }

  /// `TestApp Server`
  String get testappServer {
    return Intl.message(
      'TestApp Server',
      name: 'testappServer',
      desc: '',
      args: [],
    );
  }

  /// `E.g. myserver.com or https://dev.testapp.ga/`
  String get egMyservercomOrHttpsdevtestappga {
    return Intl.message(
      'E.g. myserver.com or https://dev.testapp.ga/',
      name: 'egMyservercomOrHttpsdevtestappga',
      desc: '',
      args: [],
    );
  }

  /// `Teacher`
  String get teacher {
    return Intl.message(
      'Teacher',
      name: 'teacher',
      desc: '',
      args: [],
    );
  }

  /// `Checking credentials...`
  String get checkingCredentials {
    return Intl.message(
      'Checking credentials...',
      name: 'checkingCredentials',
      desc: '',
      args: [],
    );
  }

  /// `Could not create this account. Let's see whether you already registered before...`
  String get couldNotCreateThisAccountLetsSeeWhetherYouAlready {
    return Intl.message(
      'Could not create this account. Let\'s see whether you already registered before...',
      name: 'couldNotCreateThisAccountLetsSeeWhetherYouAlready',
      desc: '',
      args: [],
    );
  }

  /// `There are no exercise sets yet.`
  String get thereAreNoJoinTestsYet {
    return Intl.message(
      'There are no exercise sets yet.',
      name: 'thereAreNoJoinTestsYet',
      desc: '',
      args: [],
    );
  }

  /// `Create new exercise set`
  String get createNewJoinTest {
    return Intl.message(
      'Create new exercise set',
      name: 'createNewJoinTest',
      desc: '',
      args: [],
    );
  }

  /// `JoinId`
  String get joinid {
    return Intl.message(
      'JoinId',
      name: 'joinid',
      desc: '',
      args: [],
    );
  }

  /// `If you don't have a JoinId, please ask your teacher.`
  String get ifYouDontHaveAJoinidPleaseAskYourTeacher {
    return Intl.message(
      'If you don\'t have a JoinId, please ask your teacher.',
      name: 'ifYouDontHaveAJoinidPleaseAskYourTeacher',
      desc: '',
      args: [],
    );
  }

  /// `Ooops, this JoinId is not valid for you.`
  String get ooopsThisJoinidIsNotValidForYou {
    return Intl.message(
      'Ooops, this JoinId is not valid for you.',
      name: 'ooopsThisJoinidIsNotValidForYou',
      desc: '',
      args: [],
    );
  }

  /// `Pre-sorted and focused view`
  String get presortedAndFocusedView {
    return Intl.message(
      'Pre-sorted and focused view',
      name: 'presortedAndFocusedView',
      desc: '',
      args: [],
    );
  }

  /// `Use simplified view`
  String get useSimplifiedView {
    return Intl.message(
      'Use simplified view',
      name: 'useSimplifiedView',
      desc: '',
      args: [],
    );
  }

  /// `Printable continuous text`
  String get printableContinuousText {
    return Intl.message(
      'Printable continuous text',
      name: 'printableContinuousText',
      desc: '',
      args: [],
    );
  }

  /// `View raw`
  String get viewRaw {
    return Intl.message(
      'View raw',
      name: 'viewRaw',
      desc: '',
      args: [],
    );
  }

  /// `Legal notice`
  String get legalNotice {
    return Intl.message(
      'Legal notice',
      name: 'legalNotice',
      desc: '',
      args: [],
    );
  }

  /// `Impressum gemäß der deutschen Impressumspflicht\n\nVerantwortlicher und Seitenbetreiber ist:`
  String get legalNoticeAccordingToGermanLegalNoticeLaws {
    return Intl.message(
      'Impressum gemäß der deutschen Impressumspflicht\n\nVerantwortlicher und Seitenbetreiber ist:',
      name: 'legalNoticeAccordingToGermanLegalNoticeLaws',
      desc: '',
      args: [],
    );
  }

  /// `Mail`
  String get mail {
    return Intl.message(
      'Mail',
      name: 'mail',
      desc: '',
      args: [],
    );
  }

  /// `Phone`
  String get phone {
    return Intl.message(
      'Phone',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `Mail us`
  String get mailUs {
    return Intl.message(
      'Mail us',
      name: 'mailUs',
      desc: '',
      args: [],
    );
  }

  /// `Call us`
  String get callUs {
    return Intl.message(
      'Call us',
      name: 'callUs',
      desc: '',
      args: [],
    );
  }

  /// `Legal notice`
  String get privacyPolicy {
    return Intl.message(
      'Legal notice',
      name: 'privacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `Vocabulary`
  String get vocabulary {
    return Intl.message(
      'Vocabulary',
      name: 'vocabulary',
      desc: '',
      args: [],
    );
  }

  /// `Exercise Stack Edit`
  String get exerciseStackEdit {
    return Intl.message(
      'Exercise Stack Edit',
      name: 'exerciseStackEdit',
      desc: '',
      args: [],
    );
  }

  /// `Generate exercises`
  String get generateExercises {
    return Intl.message(
      'Generate exercises',
      name: 'generateExercises',
      desc: '',
      args: [],
    );
  }

  /// `Exercise name`
  String get exerciseName {
    return Intl.message(
      'Exercise name',
      name: 'exerciseName',
      desc: '',
      args: [],
    );
  }

  /// `Exercises' name`
  String get exercisesNameGenitiv {
    return Intl.message(
      'Exercises\' name',
      name: 'exercisesNameGenitiv',
      desc: '',
      args: [],
    );
  }

  /// `Will be applied to all generated exercises`
  String get willBeAppliedToAllGeneratedExercises {
    return Intl.message(
      'Will be applied to all generated exercises',
      name: 'willBeAppliedToAllGeneratedExercises',
      desc: '',
      args: [],
    );
  }

  /// `Options`
  String get options {
    return Intl.message(
      'Options',
      name: 'options',
      desc: '',
      args: [],
    );
  }

  /// `Generate in both ways`
  String get generateInBothWays {
    return Intl.message(
      'Generate in both ways',
      name: 'generateInBothWays',
      desc: '',
      args: [],
    );
  }

  /// `Unselect this if you only want to create exercises from Language 1 to Language 2 but not vice versa.`
  String get unselectThisIfYouOnlyWantToCreateExercisesFrom {
    return Intl.message(
      'Unselect this if you only want to create exercises from Language 1 to Language 2 but not vice versa.',
      name: 'unselectThisIfYouOnlyWantToCreateExercisesFrom',
      desc: '',
      args: [],
    );
  }

  /// `Copyright`
  String get copyright {
    return Intl.message(
      'Copyright',
      name: 'copyright',
      desc: '',
      args: [],
    );
  }

  /// `I own the copyright of this exercise.`
  String get iOwnTheCopyrightOfThisExercise {
    return Intl.message(
      'I own the copyright of this exercise.',
      name: 'iOwnTheCopyrightOfThisExercise',
      desc: '',
      args: [],
    );
  }

  /// `By saving this exercise, I assign all rights of this exercise to TestApp and agree that any user and thirds will be able of reusing this exercise under the terms and conditions of CC BY-SA 4.0.`
  String get bySavingThisExerciseIAsignAllRightsOfThis {
    return Intl.message(
      'By saving this exercise, I assign all rights of this exercise to TestApp and agree that any user and thirds will be able of reusing this exercise under the terms and conditions of CC BY-SA 4.0.',
      name: 'bySavingThisExerciseIAsignAllRightsOfThis',
      desc: '',
      args: [],
    );
  }

  /// `Add one more`
  String get addOneMore {
    return Intl.message(
      'Add one more',
      name: 'addOneMore',
      desc: '',
      args: [],
    );
  }

  /// `Exercise`
  String get exercise {
    return Intl.message(
      'Exercise',
      name: 'exercise',
      desc: '',
      args: [],
    );
  }

  /// `Language 1`
  String get language1 {
    return Intl.message(
      'Language 1',
      name: 'language1',
      desc: '',
      args: [],
    );
  }

  /// `Human-readable version`
  String get humanreadableVersion {
    return Intl.message(
      'Human-readable version',
      name: 'humanreadableVersion',
      desc: '',
      args: [],
    );
  }

  /// `Language 1 RegEx`
  String get language1Regex {
    return Intl.message(
      'Language 1 RegEx',
      name: 'language1Regex',
      desc: '',
      args: [],
    );
  }

  /// `No delimiter required`
  String get noDelimiterRequired {
    return Intl.message(
      'No delimiter required',
      name: 'noDelimiterRequired',
      desc: '',
      args: [],
    );
  }

  /// `Language 2`
  String get language2 {
    return Intl.message(
      'Language 2',
      name: 'language2',
      desc: '',
      args: [],
    );
  }

  /// `Language 2 RegEx`
  String get language2Regex {
    return Intl.message(
      'Language 2 RegEx',
      name: 'language2Regex',
      desc: '',
      args: [],
    );
  }

  /// `generated`
  String get generated {
    return Intl.message(
      'generated',
      name: 'generated',
      desc: '',
      args: [],
    );
  }

  /// `Create new exercise`
  String get createNewExercise {
    return Intl.message(
      'Create new exercise',
      name: 'createNewExercise',
      desc: '',
      args: [],
    );
  }

  /// `Select exercises`
  String get selectExercises {
    return Intl.message(
      'Select exercises',
      name: 'selectExercises',
      desc: '',
      args: [],
    );
  }

  /// `Stack edit`
  String get stackEdit {
    return Intl.message(
      'Stack edit',
      name: 'stackEdit',
      desc: '',
      args: [],
    );
  }

  /// `There are no exercises for this topic.`
  String get thereAreNoExercisesForThisTopic {
    return Intl.message(
      'There are no exercises for this topic.',
      name: 'thereAreNoExercisesForThisTopic',
      desc: '',
      args: [],
    );
  }

  /// `Edit exercise`
  String get editExercise {
    return Intl.message(
      'Edit exercise',
      name: 'editExercise',
      desc: '',
      args: [],
    );
  }

  /// `Fork this test`
  String get forkThisTest {
    return Intl.message(
      'Fork this test',
      name: 'forkThisTest',
      desc: '',
      args: [],
    );
  }

  /// `Delete this exercise set...`
  String get deleteThisJoinTest {
    return Intl.message(
      'Delete this exercise set...',
      name: 'deleteThisJoinTest',
      desc: '',
      args: [],
    );
  }

  /// `Edit exercise set`
  String get editJoinTest {
    return Intl.message(
      'Edit exercise set',
      name: 'editJoinTest',
      desc: '',
      args: [],
    );
  }

  /// `Exercise set Name`
  String get joinTestName {
    return Intl.message(
      'Exercise set Name',
      name: 'joinTestName',
      desc: '',
      args: [],
    );
  }

  /// `exercises selected.`
  String get exercisesSelected {
    return Intl.message(
      'exercises selected.',
      name: 'exercisesSelected',
      desc: '',
      args: [],
    );
  }

  /// `Pick exercises`
  String get pickExercises {
    return Intl.message(
      'Pick exercises',
      name: 'pickExercises',
      desc: '',
      args: [],
    );
  }

  /// `Write this test...`
  String get writeThisTest {
    return Intl.message(
      'Write this test...',
      name: 'writeThisTest',
      desc: '',
      args: [],
    );
  }

  /// `Save exercise set`
  String get saveJoinTest {
    return Intl.message(
      'Save exercise set',
      name: 'saveJoinTest',
      desc: '',
      args: [],
    );
  }

  /// `Please fill in all fields.`
  String get pleaseFillInAllFields {
    return Intl.message(
      'Please fill in all fields.',
      name: 'pleaseFillInAllFields',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to delete this exercise set?`
  String get areYouSureToDeleteThisJoinTest {
    return Intl.message(
      'Are you sure to delete this exercise set?',
      name: 'areYouSureToDeleteThisJoinTest',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `Class`
  String get wordClass {
    return Intl.message(
      'Class',
      name: 'wordClass',
      desc: '',
      args: [],
    );
  }

  /// `Test time (in minutes)`
  String get testTimeInMinutes {
    return Intl.message(
      'Test time (in minutes)',
      name: 'testTimeInMinutes',
      desc: '',
      args: [],
    );
  }

  /// `Allow starting testing for (in minutes)`
  String get allowStartingTestingForInMinutes {
    return Intl.message(
      'Allow starting testing for (in minutes)',
      name: 'allowStartingTestingForInMinutes',
      desc: '',
      args: [],
    );
  }

  /// `Write Join Test`
  String get writeJoinTest {
    return Intl.message(
      'Write Join Test',
      name: 'writeJoinTest',
      desc: '',
      args: [],
    );
  }

  /// `Info`
  String get info {
    return Intl.message(
      'Info',
      name: 'info',
      desc: '',
      args: [],
    );
  }

  /// `Processing time`
  String get processingTime {
    return Intl.message(
      'Processing time',
      name: 'processingTime',
      desc: '',
      args: [],
    );
  }

  /// `minutes`
  String get minutes {
    return Intl.message(
      'minutes',
      name: 'minutes',
      desc: '',
      args: [],
    );
  }

  /// `Time left`
  String get timeLeft {
    return Intl.message(
      'Time left',
      name: 'timeLeft',
      desc: '',
      args: [],
    );
  }

  /// `Start personal timer`
  String get startPersonalTimer {
    return Intl.message(
      'Start personal timer',
      name: 'startPersonalTimer',
      desc: '',
      args: [],
    );
  }

  /// `Join Id`
  String get joinId {
    return Intl.message(
      'Join Id',
      name: 'joinId',
      desc: '',
      args: [],
    );
  }

  /// `Please share this id to your students.`
  String get pleaseShareThisIdToYourStudents {
    return Intl.message(
      'Please share this id to your students.',
      name: 'pleaseShareThisIdToYourStudents',
      desc: '',
      args: [],
    );
  }

  /// `Finished writing, take me to class' score`
  String get finishedWritingTakeMeToClassScore {
    return Intl.message(
      'Finished writing, take me to class\' score',
      name: 'finishedWritingTakeMeToClassScore',
      desc: '',
      args: [],
    );
  }

  /// `Activating test...`
  String get activatingTest {
    return Intl.message(
      'Activating test...',
      name: 'activatingTest',
      desc: '',
      args: [],
    );
  }

  /// `Answer order matters`
  String get answerOrderMatters {
    return Intl.message(
      'Answer order matters',
      name: 'answerOrderMatters',
      desc: '',
      args: [],
    );
  }

  /// `Multiple-choice option`
  String get multiplechoiseOption {
    return Intl.message(
      'Multiple-choice option',
      name: 'multiplechoiseOption',
      desc: '',
      args: [],
    );
  }

  /// `Important:`
  String get important {
    return Intl.message(
      'Important:',
      name: 'important',
      desc: '',
      args: [],
    );
  }

  /// `Test your regex before saving. We recommend the open-source tool regex101.`
  String get testYourRegexBeforeSavingWeRecommendTheOpensourceTool {
    return Intl.message(
      'Test your regex before saving. We recommend the open-source tool regex101.',
      name: 'testYourRegexBeforeSavingWeRecommendTheOpensourceTool',
      desc: '',
      args: [],
    );
  }

  /// `RegEx`
  String get regex {
    return Intl.message(
      'RegEx',
      name: 'regex',
      desc: '',
      args: [],
    );
  }

  /// `Edit exercise`
  String get editExericse {
    return Intl.message(
      'Edit exercise',
      name: 'editExericse',
      desc: '',
      args: [],
    );
  }

  /// `Delete this exercise`
  String get deleteThisExercise {
    return Intl.message(
      'Delete this exercise',
      name: 'deleteThisExercise',
      desc: '',
      args: [],
    );
  }

  /// `Answers`
  String get answers {
    return Intl.message(
      'Answers',
      name: 'answers',
      desc: '',
      args: [],
    );
  }

  /// `Multiple choice`
  String get multipleChoise {
    return Intl.message(
      'Multiple choice',
      name: 'multipleChoise',
      desc: '',
      args: [],
    );
  }

  /// `Input`
  String get input {
    return Intl.message(
      'Input',
      name: 'input',
      desc: '',
      args: [],
    );
  }

  /// `Input using RegEx`
  String get inputUsingRegex {
    return Intl.message(
      'Input using RegEx',
      name: 'inputUsingRegex',
      desc: '',
      args: [],
    );
  }

  /// `Please just leave empty unused text fields.`
  String get pleaseJustLeaveEmptyUnusedTextFields {
    return Intl.message(
      'Please just leave empty unused text fields.',
      name: 'pleaseJustLeaveEmptyUnusedTextFields',
      desc: '',
      args: [],
    );
  }

  /// `Exercise activation`
  String get exerciseActivation {
    return Intl.message(
      'Exercise activation',
      name: 'exerciseActivation',
      desc: '',
      args: [],
    );
  }

  /// `If you plan to write a Join Test you can temporarily disable exercises for a period of time. They won't be included in any Random Test.`
  String get ifYouPlanToWriteAJoinTestYouCan {
    return Intl.message(
      'If you plan to write a Join Test you can temporarily disable exercises for a period of time. They won\'t be included in any Random Test.',
      name: 'ifYouPlanToWriteAJoinTestYouCan',
      desc: '',
      args: [],
    );
  }

  /// `This exercise is activated. Tap to disable.`
  String get thisExerciseIsActivatedTapToDisable {
    return Intl.message(
      'This exercise is activated. Tap to disable.',
      name: 'thisExerciseIsActivatedTapToDisable',
      desc: '',
      args: [],
    );
  }

  /// `This exercise is disabled till`
  String get thisExerciseIsDisabledTill {
    return Intl.message(
      'This exercise is disabled till',
      name: 'thisExerciseIsDisabledTill',
      desc: '',
      args: [],
    );
  }

  /// `Tap to activate.`
  String get tapToActivate {
    return Intl.message(
      'Tap to activate.',
      name: 'tapToActivate',
      desc: '',
      args: [],
    );
  }

  /// `Image`
  String get image {
    return Intl.message(
      'Image',
      name: 'image',
      desc: '',
      args: [],
    );
  }

  /// `Editing images is not yet supported on mobile platforms.`
  String get editingImagesIsNotYetSupportedOnMobilePlatforms {
    return Intl.message(
      'Editing images is not yet supported on mobile platforms.',
      name: 'editingImagesIsNotYetSupportedOnMobilePlatforms',
      desc: '',
      args: [],
    );
  }

  /// `https://creativecommons.org/licenses/by-sa/4.0/`
  String get creativeCommonsUrl {
    return Intl.message(
      'https://creativecommons.org/licenses/by-sa/4.0/',
      name: 'creativeCommonsUrl',
      desc: '',
      args: [],
    );
  }

  /// `Save exercise`
  String get saveExercise {
    return Intl.message(
      'Save exercise',
      name: 'saveExercise',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to delete this Exercise?`
  String get areYouSureToDeleteThisExercise {
    return Intl.message(
      'Are you sure to delete this Exercise?',
      name: 'areYouSureToDeleteThisExercise',
      desc: '',
      args: [],
    );
  }

  /// `Add a new topic`
  String get addANewTopic {
    return Intl.message(
      'Add a new topic',
      name: 'addANewTopic',
      desc: '',
      args: [],
    );
  }

  /// `Create new topic`
  String get createNewTopic {
    return Intl.message(
      'Create new topic',
      name: 'createNewTopic',
      desc: '',
      args: [],
    );
  }

  /// `Topic name`
  String get topicName {
    return Intl.message(
      'Topic name',
      name: 'topicName',
      desc: '',
      args: [],
    );
  }

  /// `Subject`
  String get subject {
    return Intl.message(
      'Subject',
      name: 'subject',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get create {
    return Intl.message(
      'Create',
      name: 'create',
      desc: '',
      args: [],
    );
  }

  /// `Edit Class`
  String get editClass {
    return Intl.message(
      'Edit Class',
      name: 'editClass',
      desc: '',
      args: [],
    );
  }

  /// `Delete this class`
  String get deleteThisClass {
    return Intl.message(
      'Delete this class',
      name: 'deleteThisClass',
      desc: '',
      args: [],
    );
  }

  /// `Rename Class`
  String get renameClass {
    return Intl.message(
      'Rename Class',
      name: 'renameClass',
      desc: '',
      args: [],
    );
  }

  /// `Hint:`
  String get hint {
    return Intl.message(
      'Hint:',
      name: 'hint',
      desc: '',
      args: [],
    );
  }

  /// `Think about the name. Sometimes it's more sensful to give short names like `
  String get thinkAboutTheNameSometimesItsMoreSensfulToGive {
    return Intl.message(
      'Think about the name. Sometimes it\'s more sensful to give short names like ',
      name: 'thinkAboutTheNameSometimesItsMoreSensfulToGive',
      desc: '',
      args: [],
    );
  }

  /// `" but anyway, sometimes you need names like "`
  String get butAnywaySometimesYouNeedNamesLike {
    return Intl.message(
      '" but anyway, sometimes you need names like "',
      name: 'butAnywaySometimesYouNeedNamesLike',
      desc: '',
      args: [],
    );
  }

  /// `10b/Chemics`
  String get exampleClassName {
    return Intl.message(
      '10b/Chemics',
      name: 'exampleClassName',
      desc: '',
      args: [],
    );
  }

  /// `". You should talk about this with your colleagues.`
  String get youShouldTalkAboutThisWithYourColleagues {
    return Intl.message(
      '". You should talk about this with your colleagues.',
      name: 'youShouldTalkAboutThisWithYourColleagues',
      desc: '',
      args: [],
    );
  }

  /// `Class name`
  String get className {
    return Intl.message(
      'Class name',
      name: 'className',
      desc: '',
      args: [],
    );
  }

  /// `topics selected.`
  String get topicsSelected {
    return Intl.message(
      'topics selected.',
      name: 'topicsSelected',
      desc: '',
      args: [],
    );
  }

  /// `Students`
  String get students {
    return Intl.message(
      'Students',
      name: 'students',
      desc: '',
      args: [],
    );
  }

  /// `students selected.`
  String get studentsSelected {
    return Intl.message(
      'students selected.',
      name: 'studentsSelected',
      desc: '',
      args: [],
    );
  }

  /// `Save Class`
  String get saveClass {
    return Intl.message(
      'Save Class',
      name: 'saveClass',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to permanently delete this class?`
  String get areYouSureToPermanentlyDeleteThisClass {
    return Intl.message(
      'Are you sure to permanently delete this class?',
      name: 'areYouSureToPermanentlyDeleteThisClass',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get delete {
    return Intl.message(
      'Delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `No students available for this letter.`
  String get noStudentsAvailableForThisLetter {
    return Intl.message(
      'No students available for this letter.',
      name: 'noStudentsAvailableForThisLetter',
      desc: '',
      args: [],
    );
  }

  /// `Others`
  String get others {
    return Intl.message(
      'Others',
      name: 'others',
      desc: '',
      args: [],
    );
  }

  /// `Dashboard`
  String get dashboard {
    return Intl.message(
      'Dashboard',
      name: 'dashboard',
      desc: '',
      args: [],
    );
  }

  /// `More`
  String get more {
    return Intl.message(
      'More',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  /// `Overview`
  String get overview {
    return Intl.message(
      'Overview',
      name: 'overview',
      desc: '',
      args: [],
    );
  }

  /// `Please note: only the score of activated topics and students is shown.`
  String get pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents {
    return Intl.message(
      'Please note: only the score of activated topics and students is shown.',
      name: 'pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents',
      desc: '',
      args: [],
    );
  }

  /// `Not available`
  String get notAvailable {
    return Intl.message(
      'Not available',
      name: 'notAvailable',
      desc: '',
      args: [],
    );
  }

  /// `Ooops, you didn't write any Join Tests in this class yet.`
  String get ooopsYouDidntWriteAnyJoinTestsInThisClass {
    return Intl.message(
      'Ooops, you didn\'t write any Join Tests in this class yet.',
      name: 'ooopsYouDidntWriteAnyJoinTestsInThisClass',
      desc: '',
      args: [],
    );
  }

  /// `[Original test deleted.]`
  String get originalTestDeleted {
    return Intl.message(
      '[Original test deleted.]',
      name: 'originalTestDeleted',
      desc: '',
      args: [],
    );
  }

  /// `Error reading date.`
  String get errorReadingDate {
    return Intl.message(
      'Error reading date.',
      name: 'errorReadingDate',
      desc: '',
      args: [],
    );
  }

  /// `Topic`
  String get topic {
    return Intl.message(
      'Topic',
      name: 'topic',
      desc: '',
      args: [],
    );
  }

  /// `Press for more details`
  String get pressForMoreDetails {
    return Intl.message(
      'Press for more details',
      name: 'pressForMoreDetails',
      desc: '',
      args: [],
    );
  }

  /// `Create class`
  String get createClass {
    return Intl.message(
      'Create class',
      name: 'createClass',
      desc: '',
      args: [],
    );
  }

  /// `My Classes`
  String get myClasses {
    return Intl.message(
      'My Classes',
      name: 'myClasses',
      desc: '',
      args: [],
    );
  }

  /// `No classes created yet.`
  String get noClassesCreatedYet {
    return Intl.message(
      'No classes created yet.',
      name: 'noClassesCreatedYet',
      desc: '',
      args: [],
    );
  }

  /// `Create new class`
  String get createNewClass {
    return Intl.message(
      'Create new class',
      name: 'createNewClass',
      desc: '',
      args: [],
    );
  }

  /// `Wrong`
  String get wrong {
    return Intl.message(
      'Wrong',
      name: 'wrong',
      desc: '',
      args: [],
    );
  }

  /// `Correct`
  String get correct {
    return Intl.message(
      'Correct',
      name: 'correct',
      desc: '',
      args: [],
    );
  }

  /// `Score`
  String get score {
    return Intl.message(
      'Score',
      name: 'score',
      desc: '',
      args: [],
    );
  }

  /// `About TestApp`
  String get aboutTestapp {
    return Intl.message(
      'About TestApp',
      name: 'aboutTestapp',
      desc: '',
      args: [],
    );
  }

  /// `TesApp is a project offering digital education ressources. Teachers can easily manage classes, exercises and tests and save work. Students instantly ret their scores after working on exercises of any subject.`
  String get testappIsAnEducationalOpensourceProjectForWritingClassTests {
    return Intl.message(
      'TesApp is a project offering digital education ressources. Teachers can easily manage classes, exercises and tests and save work. Students instantly ret their scores after working on exercises of any subject.',
      name: 'testappIsAnEducationalOpensourceProjectForWritingClassTests',
      desc: '',
      args: [],
    );
  }

  /// `Use of TestApp is free. For schools and organizations, conditions apply.\nThe source code licensed under the terms  and conditions of the EUPL-1.2.`
  String get useOfTestappIsFreeForOrganizationsConditionsApplyntheSource {
    return Intl.message(
      'Use of TestApp is free. For schools and organizations, conditions apply.\nThe source code licensed under the terms  and conditions of the EUPL-1.2.',
      name: 'useOfTestappIsFreeForOrganizationsConditionsApplyntheSource',
      desc: '',
      args: [],
    );
  }

  /// `chaw: `
  String get chaw {
    return Intl.message(
      'chaw: ',
      name: 'chaw',
      desc: '',
      args: [],
    );
  }

  /// `nIqHom lo’, jIQochbe’ quv jIlij jIjatlhmo’ tlhIngan Hol.`
  String get niqhomLoJiqochbeQuvJilijJijatlhmoTlhinganHol {
    return Intl.message(
      'nIqHom lo’, jIQochbe’ quv jIlij jIjatlhmo’ tlhIngan Hol.',
      name: 'niqhomLoJiqochbeQuvJilijJijatlhmoTlhinganHol',
      desc: '',
      args: [],
    );
  }

  /// `https://eupl.eu/`
  String get euplUrl {
    return Intl.message(
      'https://eupl.eu/',
      name: 'euplUrl',
      desc: '',
      args: [],
    );
  }

  /// `TestApp for all devices`
  String get testappForAllDevices {
    return Intl.message(
      'TestApp for all devices',
      name: 'testappForAllDevices',
      desc: '',
      args: [],
    );
  }

  /// `Donate`
  String get donate {
    return Intl.message(
      'Donate',
      name: 'donate',
      desc: '',
      args: [],
    );
  }

  /// `Donate to TestApp open source project`
  String get donateToTestappOpenSourceProject {
    return Intl.message(
      'Donate to TestApp open source project',
      name: 'donateToTestappOpenSourceProject',
      desc: '',
      args: [],
    );
  }

  /// `Help us!`
  String get helpUs {
    return Intl.message(
      'Help us!',
      name: 'helpUs',
      desc: '',
      args: [],
    );
  }

  /// `For publishing this app on Android and Windows we need`
  String get forPublishingThisAppOnAndroidAndWindowsWeNeed {
    return Intl.message(
      'For publishing this app on Android and Windows we need',
      name: 'forPublishingThisAppOnAndroidAndWindowsWeNeed',
      desc: '',
      args: [],
    );
  }

  /// `View TestApp on Buy me a coffee`
  String get viewTestappOnBuyMeACoffee {
    return Intl.message(
      'View TestApp on Buy me a coffee',
      name: 'viewTestappOnBuyMeACoffee',
      desc: '',
      args: [],
    );
  }

  /// `Legal notice and privacy policy`
  String get legalNoticeAndPrivacyPolicy {
    return Intl.message(
      'Legal notice and privacy policy',
      name: 'legalNoticeAndPrivacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `As this application is a product of Germany, we only provide a German privacy policy according to the European and German laws.`
  String get asThisApplicationIsANoncommercialStudentProjectByA {
    return Intl.message(
      'As this application is a product of Germany, we only provide a German privacy policy according to the European and German laws.',
      name: 'asThisApplicationIsANoncommercialStudentProjectByA',
      desc: '',
      args: [],
    );
  }

  /// `Big Test (20 exercises)`
  String get bigTest20Exercises {
    return Intl.message(
      'Big Test (20 exercises)',
      name: 'bigTest20Exercises',
      desc: '',
      args: [],
    );
  }

  /// `Immense Test (50 exercises)`
  String get imenseTest50Exercises {
    return Intl.message(
      'Immense Test (50 exercises)',
      name: 'imenseTest50Exercises',
      desc: '',
      args: [],
    );
  }

  /// `App`
  String get app {
    return Intl.message(
      'App',
      name: 'app',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get language {
    return Intl.message(
      'Language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Read full privacy policy`
  String get readFullPrivacyPolicy {
    return Intl.message(
      'Read full privacy policy',
      name: 'readFullPrivacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `Repeat password`
  String get repeatPassword {
    return Intl.message(
      'Repeat password',
      name: 'repeatPassword',
      desc: '',
      args: [],
    );
  }

  /// `Last in class:`
  String get lastInClass {
    return Intl.message(
      'Last in class:',
      name: 'lastInClass',
      desc: '',
      args: [],
    );
  }

  /// `Upload image`
  String get uploadImage {
    return Intl.message(
      'Upload image',
      name: 'uploadImage',
      desc: '',
      args: [],
    );
  }

  /// `Delete image`
  String get deleteImage {
    return Intl.message(
      'Delete image',
      name: 'deleteImage',
      desc: '',
      args: [],
    );
  }

  /// `User Manual`
  String get userManual {
    return Intl.message(
      'User Manual',
      name: 'userManual',
      desc: '',
      args: [],
    );
  }

  /// `Open help menu`
  String get openHelpMenu {
    return Intl.message(
      'Open help menu',
      name: 'openHelpMenu',
      desc: '',
      args: [],
    );
  }

  /// `Help for this page`
  String get helpForThisPage {
    return Intl.message(
      'Help for this page',
      name: 'helpForThisPage',
      desc: '',
      args: [],
    );
  }

  /// `Help with Random Tests`
  String get helpWithRnadomTests {
    return Intl.message(
      'Help with Random Tests',
      name: 'helpWithRnadomTests',
      desc: '',
      args: [],
    );
  }

  /// `Wrong results/need help`
  String get wrongResultsneedHelp {
    return Intl.message(
      'Wrong results/need help',
      name: 'wrongResultsneedHelp',
      desc: '',
      args: [],
    );
  }

  /// `Help with Your Score`
  String get helpWithYourScore {
    return Intl.message(
      'Help with Your Score',
      name: 'helpWithYourScore',
      desc: '',
      args: [],
    );
  }

  /// `Help writing test`
  String get helpWritingTest {
    return Intl.message(
      'Help writing test',
      name: 'helpWritingTest',
      desc: '',
      args: [],
    );
  }

  /// `Help with Exercise Sets`
  String get helpWithJoinTests {
    return Intl.message(
      'Help with Exercise Sets',
      name: 'helpWithJoinTests',
      desc: '',
      args: [],
    );
  }

  /// `Help on Dashboard`
  String get helpOnDashboard {
    return Intl.message(
      'Help on Dashboard',
      name: 'helpOnDashboard',
      desc: '',
      args: [],
    );
  }

  /// `Help with Classes`
  String get helpWithClasses {
    return Intl.message(
      'Help with Classes',
      name: 'helpWithClasses',
      desc: '',
      args: [],
    );
  }

  /// `Help with Class Score`
  String get helpWithClassScore {
    return Intl.message(
      'Help with Class Score',
      name: 'helpWithClassScore',
      desc: '',
      args: [],
    );
  }

  /// `Help editing Class`
  String get helpEditingClass {
    return Intl.message(
      'Help editing Class',
      name: 'helpEditingClass',
      desc: '',
      args: [],
    );
  }

  /// `Help editing Exercises`
  String get helpEditingExercises {
    return Intl.message(
      'Help editing Exercises',
      name: 'helpEditingExercises',
      desc: '',
      args: [],
    );
  }

  /// `Help editing Exercise Sets`
  String get helpEditingJoinTests {
    return Intl.message(
      'Help editing Exercise Sets',
      name: 'helpEditingJoinTests',
      desc: '',
      args: [],
    );
  }

  /// `Help with Stack Edit`
  String get helpWithStackEdit {
    return Intl.message(
      'Help with Stack Edit',
      name: 'helpWithStackEdit',
      desc: '',
      args: [],
    );
  }

  /// `Uploading images is only possible after creating the exercise. Please fill in all fields, save and come back again to upload an image.`
  String get uploadingImagesIsOnlyPossibleAfterCreatingTheExercisePlease {
    return Intl.message(
      'Uploading images is only possible after creating the exercise. Please fill in all fields, save and come back again to upload an image.',
      name: 'uploadingImagesIsOnlyPossibleAfterCreatingTheExercisePlease',
      desc: '',
      args: [],
    );
  }

  /// `Help writing Join Tests`
  String get helpWitingJoinTests {
    return Intl.message(
      'Help writing Join Tests',
      name: 'helpWitingJoinTests',
      desc: '',
      args: [],
    );
  }

  /// `Open navigation menu`
  String get openNavigationMenu {
    return Intl.message(
      'Open navigation menu',
      name: 'openNavigationMenu',
      desc: '',
      args: [],
    );
  }

  /// `Next page`
  String get nextPage {
    return Intl.message(
      'Next page',
      name: 'nextPage',
      desc: '',
      args: [],
    );
  }

  /// `Previous page`
  String get previousPage {
    return Intl.message(
      'Previous page',
      name: 'previousPage',
      desc: '',
      args: [],
    );
  }

  /// `Help getting started`
  String get helpGettingStarted {
    return Intl.message(
      'Help getting started',
      name: 'helpGettingStarted',
      desc: '',
      args: [],
    );
  }

  /// `Help with Vouchers`
  String get helpWithVouchers {
    return Intl.message(
      'Help with Vouchers',
      name: 'helpWithVouchers',
      desc: '',
      args: [],
    );
  }

  /// `Help with Exercise management`
  String get helpWithExerciseManagement {
    return Intl.message(
      'Help with Exercise management',
      name: 'helpWithExerciseManagement',
      desc: '',
      args: [],
    );
  }

  /// `Help with Settings`
  String get helpWithSettings {
    return Intl.message(
      'Help with Settings',
      name: 'helpWithSettings',
      desc: '',
      args: [],
    );
  }

  /// `Learn more`
  String get learnMore {
    return Intl.message(
      'Learn more',
      name: 'learnMore',
      desc: '',
      args: [],
    );
  }

  /// `If you want to learn more about TestApp, help our open source project or find TestApp for other devices, you can visit our online documentation here.`
  String get ifYouWantToLearnMoreAboutTestappHelpOur {
    return Intl.message(
      'If you want to learn more about TestApp, help our open source project or find TestApp for other devices, you can visit our online documentation here.',
      name: 'ifYouWantToLearnMoreAboutTestappHelpOur',
      desc: '',
      args: [],
    );
  }

  /// `Daily practice notification`
  String get dailyPracticeNotification {
    return Intl.message(
      'Daily practice notification',
      name: 'dailyPracticeNotification',
      desc: '',
      args: [],
    );
  }

  /// `A motivating message to keep you practicing.`
  String get aMotivatingMessageToKeepYouPracticing {
    return Intl.message(
      'A motivating message to keep you practicing.',
      name: 'aMotivatingMessageToKeepYouPracticing',
      desc: '',
      args: [],
    );
  }

  /// `TestApp daily practice`
  String get testappDailyPractice {
    return Intl.message(
      'TestApp daily practice',
      name: 'testappDailyPractice',
      desc: '',
      args: [],
    );
  }

  /// `Time to practice some `
  String get timeToPracticeSome {
    return Intl.message(
      'Time to practice some ',
      name: 'timeToPracticeSome',
      desc: '',
      args: [],
    );
  }

  /// ` today 🎉`
  String get today {
    return Intl.message(
      ' today 🎉',
      name: 'today',
      desc: '',
      args: [],
    );
  }

  /// `Enable motivating push reminder.`
  String get enableMotivationgPushRemminder {
    return Intl.message(
      'Enable motivating push reminder.',
      name: 'enableMotivationgPushRemminder',
      desc: '',
      args: [],
    );
  }

  /// `stuff`
  String get stuff {
    return Intl.message(
      'stuff',
      name: 'stuff',
      desc: '',
      args: [],
    );
  }

  /// `Rate TestApp`
  String get rateTestapp {
    return Intl.message(
      'Rate TestApp',
      name: 'rateTestapp',
      desc: '',
      args: [],
    );
  }

  /// `To prevent abuse of teacher accounts, you need to insert a voucher. You can get a voucher from any already registered teacher of your school or by your school's admin.`
  String get toPreventAbuseOfTeacherAccountsYouNeedToInsert {
    return Intl.message(
      'To prevent abuse of teacher accounts, you need to insert a voucher. You can get a voucher from any already registered teacher of your school or by your school\'s admin.',
      name: 'toPreventAbuseOfTeacherAccountsYouNeedToInsert',
      desc: '',
      args: [],
    );
  }

  /// `Note: RegEx are for advanced users only. Do not use them if you do not know RegEx.`
  String get noteRegexAreForAdvancedUsersOnlyDoNotUse {
    return Intl.message(
      'Note: RegEx are for advanced users only. Do not use them if you do not know RegEx.',
      name: 'noteRegexAreForAdvancedUsersOnlyDoNotUse',
      desc: '',
      args: [],
    );
  }

  /// `What is TestApp?`
  String get newToTestapp {
    return Intl.message(
      'What is TestApp?',
      name: 'newToTestapp',
      desc: '',
      args: [],
    );
  }

  /// `Tap to learn more about TestApp on our information website.`
  String get tapToLearnMoreAboutTestappOnOurInformationWebsite {
    return Intl.message(
      'Tap to learn more about TestApp on our information website.',
      name: 'tapToLearnMoreAboutTestappOnOurInformationWebsite',
      desc: '',
      args: [],
    );
  }

  /// `https://www.testapp.ga/en/`
  String get testAppWebSite {
    return Intl.message(
      'https://www.testapp.ga/en/',
      name: 'testAppWebSite',
      desc: '',
      args: [],
    );
  }

  /// `Website`
  String get website {
    return Intl.message(
      'Website',
      name: 'website',
      desc: '',
      args: [],
    );
  }

  /// `Use as assignment`
  String get useAsAssignment {
    return Intl.message(
      'Use as assignment',
      name: 'useAsAssignment',
      desc: '',
      args: [],
    );
  }

  /// `Assignments`
  String get assignments {
    return Intl.message(
      'Assignments',
      name: 'assignments',
      desc: '',
      args: [],
    );
  }

  /// `Help with assignments`
  String get helpWithAssignments {
    return Intl.message(
      'Help with assignments',
      name: 'helpWithAssignments',
      desc: '',
      args: [],
    );
  }

  /// `Hoary, there are currently no assignments for you!`
  String get huaryThereAreCurrentlyNoAssignmentsForYou {
    return Intl.message(
      'Hoary, there are currently no assignments for you!',
      name: 'huaryThereAreCurrentlyNoAssignmentsForYou',
      desc: '',
      args: [],
    );
  }

  /// `Submit Assignment`
  String get submitAssignment {
    return Intl.message(
      'Submit Assignment',
      name: 'submitAssignment',
      desc: '',
      args: [],
    );
  }

  /// `% completed`
  String get completed {
    return Intl.message(
      '% completed',
      name: 'completed',
      desc: '',
      args: [],
    );
  }

  /// `This assignment is expired but you didn't submit anything yet.`
  String get thisAssignmentIsExpriedButYouDidntSubmitAnythingYet {
    return Intl.message(
      'This assignment is expired but you didn\'t submit anything yet.',
      name: 'thisAssignmentIsExpriedButYouDidntSubmitAnythingYet',
      desc: '',
      args: [],
    );
  }

  /// `You can still submit the assignment but you won't be able to edit your submission later. Moreover, your teacher will see that you submitted delayed.`
  String get youCanStillSubmitTheAssignmentButYouWontBe {
    return Intl.message(
      'You can still submit the assignment but you won\'t be able to edit your submission later. Moreover, your teacher will see that you submitted delayed.',
      name: 'youCanStillSubmitTheAssignmentButYouWontBe',
      desc: '',
      args: [],
    );
  }

  /// `Work on assignment`
  String get workOnAssignment {
    return Intl.message(
      'Work on assignment',
      name: 'workOnAssignment',
      desc: '',
      args: [],
    );
  }

  /// `View score`
  String get viewScore {
    return Intl.message(
      'View score',
      name: 'viewScore',
      desc: '',
      args: [],
    );
  }

  /// `Join Tests`
  String get joinTestsReally {
    return Intl.message(
      'Join Tests',
      name: 'joinTestsReally',
      desc: '',
      args: [],
    );
  }

  /// `Ooops, you didn't create any assignment for this class yet. Please select an Exercise Set to create an assignment,`
  String get ooopsYouDidntCreateAnyAssignmentForThisClassYet {
    return Intl.message(
      'Ooops, you didn\'t create any assignment for this class yet. Please select an Exercise Set to create an assignment,',
      name: 'ooopsYouDidntCreateAnyAssignmentForThisClassYet',
      desc: '',
      args: [],
    );
  }

  /// `Select Exercise Set`
  String get selectExerciseSet {
    return Intl.message(
      'Select Exercise Set',
      name: 'selectExerciseSet',
      desc: '',
      args: [],
    );
  }

  /// `Assignment Details`
  String get assignmentDetails {
    return Intl.message(
      'Assignment Details',
      name: 'assignmentDetails',
      desc: '',
      args: [],
    );
  }

  /// `No one submitted any result yet.`
  String get nooneSubmittedAnyResultYet {
    return Intl.message(
      'No one submitted any result yet.',
      name: 'nooneSubmittedAnyResultYet',
      desc: '',
      args: [],
    );
  }

  /// `Average`
  String get average {
    return Intl.message(
      'Average',
      name: 'average',
      desc: '',
      args: [],
    );
  }

  /// `Submissions`
  String get submittions {
    return Intl.message(
      'Submissions',
      name: 'submittions',
      desc: '',
      args: [],
    );
  }

  /// `This submission was late.`
  String get thisSubmissionWasLate {
    return Intl.message(
      'This submission was late.',
      name: 'thisSubmissionWasLate',
      desc: '',
      args: [],
    );
  }

  /// `Submitted `
  String get submitted {
    return Intl.message(
      'Submitted ',
      name: 'submitted',
      desc: '',
      args: [],
    );
  }

  /// `% correct answers`
  String get correctAnswers {
    return Intl.message(
      '% correct answers',
      name: 'correctAnswers',
      desc: '',
      args: [],
    );
  }

  /// `% are completed and correct`
  String get areCompletedAndCorrect {
    return Intl.message(
      '% are completed and correct',
      name: 'areCompletedAndCorrect',
      desc: '',
      args: [],
    );
  }

  /// `View detailed score`
  String get viewDetailedScore {
    return Intl.message(
      'View detailed score',
      name: 'viewDetailedScore',
      desc: '',
      args: [],
    );
  }

  /// `Assignment title`
  String get assignmentTitle {
    return Intl.message(
      'Assignment title',
      name: 'assignmentTitle',
      desc: '',
      args: [],
    );
  }

  /// `Assignment content`
  String get assignmentContent {
    return Intl.message(
      'Assignment content',
      name: 'assignmentContent',
      desc: '',
      args: [],
    );
  }

  /// `Time to work on`
  String get timeToWorkOn {
    return Intl.message(
      'Time to work on',
      name: 'timeToWorkOn',
      desc: '',
      args: [],
    );
  }

  /// `In days`
  String get inDays {
    return Intl.message(
      'In days',
      name: 'inDays',
      desc: '',
      args: [],
    );
  }

  /// `Create assignment`
  String get createAssignment {
    return Intl.message(
      'Create assignment',
      name: 'createAssignment',
      desc: '',
      args: [],
    );
  }

  /// `In total, `
  String get inTotal {
    return Intl.message(
      'In total, ',
      name: 'inTotal',
      desc: '',
      args: [],
    );
  }

  /// `GbR (civil law association)`
  String get gbr {
    return Intl.message(
      'GbR (civil law association)',
      name: 'gbr',
      desc: '',
      args: [],
    );
  }

  /// `Associates: Jasper Michalke`
  String get associatesJasperMichalke {
    return Intl.message(
      'Associates: Jasper Michalke',
      name: 'associatesJasperMichalke',
      desc: '',
      args: [],
    );
  }

  /// `School-wide topic grade overwrites`
  String get schoolwideTopicGradeOverwrites {
    return Intl.message(
      'School-wide topic grade overwrites',
      name: 'schoolwideTopicGradeOverwrites',
      desc: '',
      args: [],
    );
  }

  /// `As curriculum varies - even within one state - every school can define custom grades for any topic. Please use the sliders below to set the grade of any topic.`
  String get asCurriculumVarriesEvenWithinOneStateEverySchoolCan {
    return Intl.message(
      'As curriculum varies - even within one state - every school can define custom grades for any topic. Please use the sliders below to set the grade of any topic.',
      name: 'asCurriculumVarriesEvenWithinOneStateEverySchoolCan',
      desc: '',
      args: [],
    );
  }

  /// `Grade`
  String get grade {
    return Intl.message(
      'Grade',
      name: 'grade',
      desc: '',
      args: [],
    );
  }

  /// `Assign to grade `
  String get assignToGrade {
    return Intl.message(
      'Assign to grade ',
      name: 'assignToGrade',
      desc: '',
      args: [],
    );
  }

  /// `Do not assign to any grade.`
  String get doNotAssignToAnyGrade {
    return Intl.message(
      'Do not assign to any grade.',
      name: 'doNotAssignToAnyGrade',
      desc: '',
      args: [],
    );
  }

  /// `G. `
  String get g {
    return Intl.message(
      'G. ',
      name: 'g',
      desc: '',
      args: [],
    );
  }

  /// `Edit grade affiliation`
  String get editGradeAffiliation {
    return Intl.message(
      'Edit grade affiliation',
      name: 'editGradeAffiliation',
      desc: '',
      args: [],
    );
  }

  /// `Without grade affiliation`
  String get withoutGradeAffiliation {
    return Intl.message(
      'Without grade affiliation',
      name: 'withoutGradeAffiliation',
      desc: '',
      args: [],
    );
  }

  /// `Note: It is strictly prohibited to upload any image depicting real people, especially students.`
  String get noteItIsStrictlyProhibitedToUploadAnyImageDepicting {
    return Intl.message(
      'Note: It is strictly prohibited to upload any image depicting real people, especially students.',
      name: 'noteItIsStrictlyProhibitedToUploadAnyImageDepicting',
      desc: '',
      args: [],
    );
  }

  /// `Practise`
  String get practise {
    return Intl.message(
      'Practise',
      name: 'practise',
      desc: '',
      args: [],
    );
  }

  /// `Help with practise tests`
  String get helpWithPractiseTests {
    return Intl.message(
      'Help with practise tests',
      name: 'helpWithPractiseTests',
      desc: '',
      args: [],
    );
  }

  /// `Search exercises`
  String get searchExercises {
    return Intl.message(
      'Search exercises',
      name: 'searchExercises',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a search term.`
  String get pleaseEnterASearchTerm {
    return Intl.message(
      'Please enter a search term.',
      name: 'pleaseEnterASearchTerm',
      desc: '',
      args: [],
    );
  }

  /// `No matching exercises found.`
  String get noMatchingExercisesFound {
    return Intl.message(
      'No matching exercises found.',
      name: 'noMatchingExercisesFound',
      desc: '',
      args: [],
    );
  }

  /// `View full exercise`
  String get viewFullExercise {
    return Intl.message(
      'View full exercise',
      name: 'viewFullExercise',
      desc: '',
      args: [],
    );
  }

  /// `Clear`
  String get clear {
    return Intl.message(
      'Clear',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Help with exercise solutions`
  String get helpWithExerciseSolutions {
    return Intl.message(
      'Help with exercise solutions',
      name: 'helpWithExerciseSolutions',
      desc: '',
      args: [],
    );
  }

  /// `Step-by-step solution`
  String get stepbystepSolution {
    return Intl.message(
      'Step-by-step solution',
      name: 'stepbystepSolution',
      desc: '',
      args: [],
    );
  }

  /// `About this exercise`
  String get aboutThisExercise {
    return Intl.message(
      'About this exercise',
      name: 'aboutThisExercise',
      desc: '',
      args: [],
    );
  }

  /// `Exercise No.`
  String get exerciseNo {
    return Intl.message(
      'Exercise No.',
      name: 'exerciseNo',
      desc: '',
      args: [],
    );
  }

  /// `Copied id to clipboard.`
  String get copiedIdToClipboard {
    return Intl.message(
      'Copied id to clipboard.',
      name: 'copiedIdToClipboard',
      desc: '',
      args: [],
    );
  }

  /// `Tap to copy.`
  String get tapToCopy {
    return Intl.message(
      'Tap to copy.',
      name: 'tapToCopy',
      desc: '',
      args: [],
    );
  }

  /// `Solution & tags (optional)`
  String get solutionTags {
    return Intl.message(
      'Solution & tags (optional)',
      name: 'solutionTags',
      desc: '',
      args: [],
    );
  }

  /// `You can provide a step-by-step solution for this exercise. This might be helpful for students to understand their mistakes after working on exercises.\nThe solution will not be shown while writing a test.`
  String get youCanProvideAStepbystepSolutionForThisExerciseThis {
    return Intl.message(
      'You can provide a step-by-step solution for this exercise. This might be helpful for students to understand their mistakes after working on exercises.\nThe solution will not be shown while writing a test.',
      name: 'youCanProvideAStepbystepSolutionForThisExerciseThis',
      desc: '',
      args: [],
    );
  }

  /// `Tags can help to find this exercise later.`
  String get tagsCanHelpToFindThisExerciseLater {
    return Intl.message(
      'Tags can help to find this exercise later.',
      name: 'tagsCanHelpToFindThisExerciseLater',
      desc: '',
      args: [],
    );
  }

  /// `Add tags`
  String get addTags {
    return Intl.message(
      'Add tags',
      name: 'addTags',
      desc: '',
      args: [],
    );
  }

  /// `E.g. radiation, derivative, etc.`
  String get egRadiationDerivativeEtc {
    return Intl.message(
      'E.g. radiation, derivative, etc.',
      name: 'egRadiationDerivativeEtc',
      desc: '',
      args: [],
    );
  }

  /// `Add tag`
  String get addTag {
    return Intl.message(
      'Add tag',
      name: 'addTag',
      desc: '',
      args: [],
    );
  }

  /// `Error saving the exercises`
  String get errorSavingTheExercises {
    return Intl.message(
      'Error saving the exercises',
      name: 'errorSavingTheExercises',
      desc: '',
      args: [],
    );
  }

  /// `Did someone cut the wire?`
  String get errorDuringCommunicationWithServer {
    return Intl.message(
      'Did someone cut the wire?',
      name: 'errorDuringCommunicationWithServer',
      desc: '',
      args: [],
    );
  }

  /// `Details`
  String get details {
    return Intl.message(
      'Details',
      name: 'details',
      desc: '',
      args: [],
    );
  }

  /// `Contact us`
  String get contactUs {
    return Intl.message(
      'Contact us',
      name: 'contactUs',
      desc: '',
      args: [],
    );
  }

  /// `Retry`
  String get retry {
    return Intl.message(
      'Retry',
      name: 'retry',
      desc: '',
      args: [],
    );
  }

  /// `We know the following: `
  String get weKnowTheFollowing {
    return Intl.message(
      'We know the following: ',
      name: 'weKnowTheFollowing',
      desc: '',
      args: [],
    );
  }

  /// `Unfortunately, we have no further information.`
  String get unfortunatelyWeHaveNoFurtherInformation {
    return Intl.message(
      'Unfortunately, we have no further information.',
      name: 'unfortunatelyWeHaveNoFurtherInformation',
      desc: '',
      args: [],
    );
  }

  /// `We unfortunately could not communicate with our server.`
  String get weUnfortunatelyCouldNotCommunicateWithOurServer {
    return Intl.message(
      'We unfortunately could not communicate with our server.',
      name: 'weUnfortunatelyCouldNotCommunicateWithOurServer',
      desc: '',
      args: [],
    );
  }

  /// `Hello`
  String get hello {
    return Intl.message(
      'Hello',
      name: 'hello',
      desc: '',
      args: [],
    );
  }

  /// `How would you like to start?`
  String get howWouldYouLikeToStart {
    return Intl.message(
      'How would you like to start?',
      name: 'howWouldYouLikeToStart',
      desc: '',
      args: [],
    );
  }

  /// `Manage exercises`
  String get manageExercises {
    return Intl.message(
      'Manage exercises',
      name: 'manageExercises',
      desc: '',
      args: [],
    );
  }

  /// `You can view exercises, edit them or create new ones. They can later be used in Exercise Sets for class tests or as homework. Moreover, students can freely select topics for their practice.`
  String get youCanViewExercisesEditThemOrCreateNewOnes {
    return Intl.message(
      'You can view exercises, edit them or create new ones. They can later be used in Exercise Sets for class tests or as homework. Moreover, students can freely select topics for their practice.',
      name: 'youCanViewExercisesEditThemOrCreateNewOnes',
      desc: '',
      args: [],
    );
  }

  /// `Create new exercises`
  String get createNewExercises {
    return Intl.message(
      'Create new exercises',
      name: 'createNewExercises',
      desc: '',
      args: [],
    );
  }

  /// `If you need to create a single exercise, you can add one below.`
  String get ifYouNeedToCreateASingleExerciseYouCan {
    return Intl.message(
      'If you need to create a single exercise, you can add one below.',
      name: 'ifYouNeedToCreateASingleExerciseYouCan',
      desc: '',
      args: [],
    );
  }

  /// `Create a new exercise`
  String get createANewExercise {
    return Intl.message(
      'Create a new exercise',
      name: 'createANewExercise',
      desc: '',
      args: [],
    );
  }

  /// `Create new exercises in stack mode`
  String get createNewExercisesInStackMode {
    return Intl.message(
      'Create new exercises in stack mode',
      name: 'createNewExercisesInStackMode',
      desc: '',
      args: [],
    );
  }

  /// `Stack mode is perfect if you want to create many similar exercises at once; for example vocabulary exercises for a certain topic or simple calculation exercises for math or physics.`
  String get stackModeIsPerfectIfYouWantToCreateMany {
    return Intl.message(
      'Stack mode is perfect if you want to create many similar exercises at once; for example vocabulary exercises for a certain topic or simple calculation exercises for math or physics.',
      name: 'stackModeIsPerfectIfYouWantToCreateMany',
      desc: '',
      args: [],
    );
  }

  /// `Create in stack mode`
  String get createInStackMode {
    return Intl.message(
      'Create in stack mode',
      name: 'createInStackMode',
      desc: '',
      args: [],
    );
  }

  /// `Browse existing exercises by subject or topic`
  String get browseExistingExercisesBySubjectOrTopic {
    return Intl.message(
      'Browse existing exercises by subject or topic',
      name: 'browseExistingExercisesBySubjectOrTopic',
      desc: '',
      args: [],
    );
  }

  /// `Exercises are organized by subject, topic and grade. Go to the exercises overview to browse them by topics.`
  String get exercisesAreOrganizedBySubjectTopicAndGradeGoTo {
    return Intl.message(
      'Exercises are organized by subject, topic and grade. Go to the exercises overview to browse them by topics.',
      name: 'exercisesAreOrganizedBySubjectTopicAndGradeGoTo',
      desc: '',
      args: [],
    );
  }

  /// `View exercises`
  String get viewExercises {
    return Intl.message(
      'View exercises',
      name: 'viewExercises',
      desc: '',
      args: [],
    );
  }

  /// `Exam and control`
  String get examAndControl {
    return Intl.message(
      'Exam and control',
      name: 'examAndControl',
      desc: '',
      args: [],
    );
  }

  /// `See your student's scores by topic and get their submissions to Assignments or Join Tests.`
  String get seeYourStudentsScoresByTopicAndGetTheirSubmissions {
    return Intl.message(
      'See your student\'s scores by topic and get their submissions to Assignments or Join Tests.',
      name: 'seeYourStudentsScoresByTopicAndGetTheirSubmissions',
      desc: '',
      args: [],
    );
  }

  /// `View your classes score or edit them`
  String get viewYourClassesScoreOrEditThem {
    return Intl.message(
      'View your classes score or edit them',
      name: 'viewYourClassesScoreOrEditThem',
      desc: '',
      args: [],
    );
  }

  /// `Manage classes`
  String get manageClasses {
    return Intl.message(
      'Manage classes',
      name: 'manageClasses',
      desc: '',
      args: [],
    );
  }

  /// `If you want to write class papers or assign homework, you manage the corresponding exercises in Exercise Sets. An Exercise Set can be used for any class by you or your colleagues Once created, they can be used for homework or as join tests.`
  String get ifYouWantToWriteClassPapersOrAssignHomework {
    return Intl.message(
      'If you want to write class papers or assign homework, you manage the corresponding exercises in Exercise Sets. An Exercise Set can be used for any class by you or your colleagues Once created, they can be used for homework or as join tests.',
      name: 'ifYouWantToWriteClassPapersOrAssignHomework',
      desc: '',
      args: [],
    );
  }

  /// `Manage exercise sets`
  String get manageExerciseSets {
    return Intl.message(
      'Manage exercise sets',
      name: 'manageExerciseSets',
      desc: '',
      args: [],
    );
  }

  /// `Here, you can create, delete and view your and your colleague's exercise sets for later use in Join Tests or Assignments.`
  String get hereYouCanCreateDeleteAndViewYourAndYour {
    return Intl.message(
      'Here, you can create, delete and view your and your colleague\'s exercise sets for later use in Join Tests or Assignments.',
      name: 'hereYouCanCreateDeleteAndViewYourAndYour',
      desc: '',
      args: [],
    );
  }

  /// `View Exercise Sets`
  String get viewExerciseSets {
    return Intl.message(
      'View Exercise Sets',
      name: 'viewExerciseSets',
      desc: '',
      args: [],
    );
  }

  /// `Assign homework to a class`
  String get assignHomeworkToAClass {
    return Intl.message(
      'Assign homework to a class',
      name: 'assignHomeworkToAClass',
      desc: '',
      args: [],
    );
  }

  /// `You can assign Exercise Sets to your class for a fixed amount of days they can work on it. Even after your students started working, they can save their work and continue the exercises later.`
  String get youCanAssignExerciseSetsToYourClassForA {
    return Intl.message(
      'You can assign Exercise Sets to your class for a fixed amount of days they can work on it. Even after your students started working, they can save their work and continue the exercises later.',
      name: 'youCanAssignExerciseSetsToYourClassForA',
      desc: '',
      args: [],
    );
  }

  /// `Write a join test`
  String get writeAJoinTest {
    return Intl.message(
      'Write a join test',
      name: 'writeAJoinTest',
      desc: '',
      args: [],
    );
  }

  /// `A Join Test represents a check-up, a vocabulary test or a class paper in digital. Students work on an Exercise Set for a certain time and you can immediately see their responses and scores.`
  String get aJoinTestRepresentsACheckupAVocabularyTestOr {
    return Intl.message(
      'A Join Test represents a check-up, a vocabulary test or a class paper in digital. Students work on an Exercise Set for a certain time and you can immediately see their responses and scores.',
      name: 'aJoinTestRepresentsACheckupAVocabularyTestOr',
      desc: '',
      args: [],
    );
  }

  /// `Classes represent your students in your digital classroom. You can assign homework, class papers or topics for practicing to your classes or manage the students of your class. For every topic your class is working on, you get details statistics about your students and their practicing scores.`
  String get classesRepresentYourStudentsInYourDigitalClassroomYouCan {
    return Intl.message(
      'Classes represent your students in your digital classroom. You can assign homework, class papers or topics for practicing to your classes or manage the students of your class. For every topic your class is working on, you get details statistics about your students and their practicing scores.',
      name: 'classesRepresentYourStudentsInYourDigitalClassroomYouCan',
      desc: '',
      args: [],
    );
  }

  /// `If you want to check out existing exercises or see the step-by-step solutions, you can search for exercises by tags and keywords.`
  String get ifYouWantToCheckOutExistingExercisesOrSee {
    return Intl.message(
      'If you want to check out existing exercises or see the step-by-step solutions, you can search for exercises by tags and keywords.',
      name: 'ifYouWantToCheckOutExistingExercisesOrSee',
      desc: '',
      args: [],
    );
  }

  /// `Search for exercises`
  String get searchForExercises {
    return Intl.message(
      'Search for exercises',
      name: 'searchForExercises',
      desc: '',
      args: [],
    );
  }

  /// `assets/Google Play EN.png`
  String get assetsgooglePlayEnpng {
    return Intl.message(
      'assets/Google Play EN.png',
      name: 'assetsgooglePlayEnpng',
      desc: '',
      args: [],
    );
  }

  /// `unknown`
  String get unknown {
    return Intl.message(
      'unknown',
      name: 'unknown',
      desc: '',
      args: [],
    );
  }

  /// `A product of TestApp.schule`
  String get aProductOfTestappschule {
    return Intl.message(
      'A product of TestApp.schule',
      name: 'aProductOfTestappschule',
      desc: '',
      args: [],
    );
  }

  /// `Login as student or teacher`
  String get loginAlStudentOrTeacher {
    return Intl.message(
      'Login as student or teacher',
      name: 'loginAlStudentOrTeacher',
      desc: '',
      args: [],
    );
  }

  /// `You didn't create any Exercise Set yet.`
  String get youDidntCreateAnyExerciseSetYet {
    return Intl.message(
      'You didn\'t create any Exercise Set yet.',
      name: 'youDidntCreateAnyExerciseSetYet',
      desc: '',
      args: [],
    );
  }

  /// `Own`
  String get own {
    return Intl.message(
      'Own',
      name: 'own',
      desc: '',
      args: [],
    );
  }

  /// `All`
  String get all {
    return Intl.message(
      'All',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `For students`
  String get forStudents {
    return Intl.message(
      'For students',
      name: 'forStudents',
      desc: '',
      args: [],
    );
  }

  /// `General`
  String get general {
    return Intl.message(
      'General',
      name: 'general',
      desc: '',
      args: [],
    );
  }

  /// `For teachers`
  String get forTeachers {
    return Intl.message(
      'For teachers',
      name: 'forTeachers',
      desc: '',
      args: [],
    );
  }

  /// `You did not create any class yet`
  String get youDidNotCreateAnyClassYet {
    return Intl.message(
      'You did not create any class yet',
      name: 'youDidNotCreateAnyClassYet',
      desc: '',
      args: [],
    );
  }

  /// `If you want to use an exercise set, you need to create a class first. Otherwise, we would not be able to show the student's results.`
  String get ifYouWantToUseAnExerciseSetYouNeed {
    return Intl.message(
      'If you want to use an exercise set, you need to create a class first. Otherwise, we would not be able to show the student\'s results.',
      name: 'ifYouWantToUseAnExerciseSetYouNeed',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'de'),
      Locale.fromSubtags(languageCode: 'fr'),
      Locale.fromSubtags(languageCode: 'tlh'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}