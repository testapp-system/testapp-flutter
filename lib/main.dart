import 'package:flutter/material.dart';
import 'package:test_app_flutter/slpashScreen.dart';

import 'generated/l10n.dart';

bool isAdmin = true;

void main() => runApp(new TestApp());

class TestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TestApp',
      localizationsDelegates: [S.delegate],
      supportedLocales: [
        Locale.fromSubtags(languageCode: 'en'),
      ],
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.lightBlue,
        primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
        buttonTheme: ButtonThemeData(
            textTheme: ButtonTextTheme.primary,
            buttonColor: Colors.green,
            colorScheme: ColorScheme.light(primary: Colors.green)),
        tabBarTheme: TabBarTheme(
            labelColor: Colors.white,
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 4, color: Colors.green))),
        primaryIconTheme: IconThemeData(color: Colors.white),
        accentColor: Colors.green,
        textTheme: TextTheme(
          headline6: TextStyle(fontFamily: 'PT Sans Narrow'),
          headline5: TextStyle(fontFamily: 'PT Sans Narrow'),
          headline4: TextStyle(fontFamily: 'Poiret One'),
          headline3: TextStyle(fontFamily: 'Poiret One'),
          headline2: TextStyle(fontFamily: 'Poiret One'),
          headline1: TextStyle(fontFamily: 'Poiret One'),
        ),
        appBarTheme: AppBarTheme(
          textTheme: TextTheme(
            headline6: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white,
                fontWeight: FontWeight.w500,
                fontSize: 20,
                letterSpacing: 0.15),
          ),
        ),
        dividerTheme: DividerThemeData(space: 2),
        cardTheme: CardTheme(
            color: Colors.white,
            margin: EdgeInsets.fromLTRB(32, 16, 32, 16),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            elevation: 1),
        fontFamily: 'Montserrat',
      ),
      darkTheme: ThemeData(
        bottomNavigationBarTheme:
            BottomNavigationBarThemeData(backgroundColor: Colors.black),
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue,
        primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.black)),
        buttonTheme: ButtonThemeData(
            textTheme: ButtonTextTheme.primary,
            buttonColor: Colors.green,
            colorScheme: ColorScheme.light(primary: Colors.green)),
        buttonColor: Colors.white,
        tabBarTheme: TabBarTheme(
            labelColor: Colors.black,
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 4, color: Colors.green))),
        primaryIconTheme: IconThemeData(color: Colors.black),
        accentColor: Colors.green,
        textTheme: TextTheme(
          headline6: TextStyle(fontFamily: 'PT Sans Narrow'),
          headline5: TextStyle(fontFamily: 'PT Sans Narrow'),
          headline4: TextStyle(fontFamily: 'Poiret One'),
          headline3: TextStyle(fontFamily: 'Poiret One'),
          headline2: TextStyle(fontFamily: 'Poiret One'),
          headline1: TextStyle(fontFamily: 'Poiret One'),
          subtitle1: TextStyle(fontFamily: 'Montserrat'),
          subtitle2: TextStyle(fontFamily: 'Montserrat'),
        ),
        appBarTheme: AppBarTheme(
          textTheme: TextTheme(
            headline6: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 20,
                letterSpacing: 0.15),
          ),
        ),
        dividerTheme: DividerThemeData(space: 2),
        cardTheme: CardTheme(
            //color: Colors.black,
            margin: EdgeInsets.fromLTRB(32, 16, 32, 16),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            elevation: 1),
        fontFamily: 'Montserrat',
      ),
      home: SplashScreen(),
    );
  }
}
