library test_app_flutter.globals;

import 'package:flutter/material.dart';

import 'api.dart';
import 'style.dart';

TestAppI api;

bool isLoggedIn = false;
bool isAdmin = false;
double score = 0;
Widget dashboardBear = RandomBear(
  scale: 1.5,
);
Map userInfo = {};
