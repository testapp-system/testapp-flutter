import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/chart.dart';
import 'package:test_app_flutter/classes.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editExercise.dart';
import 'package:test_app_flutter/exerciseSets.dart';
import 'package:test_app_flutter/exerciseStackEdit.dart';
import 'package:test_app_flutter/exercises.dart';
import 'package:test_app_flutter/login.dart';
import 'package:test_app_flutter/practise.dart';
import 'package:test_app_flutter/searchExercise.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/userScore.dart';

import 'api.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var api = globals.api;
  bool _scoreLoaded = false;
  double _score = globals.score;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    loadScore();
    adminCheck(context).then(((data) {
      setState(() {});
    }));
    super.initState();
  }

  Future<void> loadScore() async {
    api.call('userScore', context: context).then((data) {
      setState(() {
        _scoreLoaded = true;
        if (!(data['response'] is bool)) {
          globals.score = data['response'] * 100;
        } else {
          globals.score = -1;
        }
        Preferences().save("score", globals.score.toString());
        _score = globals.score;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var chart = (_score != -1)
        ? (_scoreLoaded)
            ? new ScoreChart(_score)
            : CenterProgress()
        : ListTile(
            leading: FaIcon(FontAwesomeIcons.infoCircle),
            title: Text(S.of(context).youDidntWriteAnyTestYet),
            trailing: OutlinedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (b) => Practise()));
              },
              child: Text(S.of(context).writeTest),
            ));
    return ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.dashboard, label: S.of(context).helpOnDashboard),
      body: TestAppScrollBar(
        controller: _scrollController,
        child: (globals.isAdmin)
            ? TeacherDashboardGrid(scrollController: _scrollController)
            : ListView(
                controller: _scrollController,
                children: [
                  TestAppCard(
                    children: <Widget>[
                      chart,
                      ButtonBar(
                        children: <Widget>[
                          ElevatedButton(
                            child: Text(S.of(context).more),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => UserScore()));
                            },
                          )
                        ],
                      )
                    ],
                  ),
                  TestAppCard(children: [
                    Center(
                        child: SizedBox(
                            height: 320,
                            child: Hero(
                                tag: "centralBear",
                                child: globals.dashboardBear)))
                  ])
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: S.of(context).practise,
        child: FaIcon(FontAwesomeIcons.dumbbell),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Practise()),
          );
        },
      ),
    );
  }
}

class TeacherDashboardGrid extends StatefulWidget {
  final ScrollController scrollController;

  const TeacherDashboardGrid({Key key, this.scrollController})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _TeacherDashboardGridState();
}

class _TeacherDashboardGridState extends State<TeacherDashboardGrid> {
  double kBoxWidth = 468;
  String greeting = '';

  @override
  void initState() {
    globals.api.call('userInfo', context: context).then((value) {
      setState(() {
        greeting = ', ' + value['response']['name'];
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(controller: widget.scrollController, children: [
      Container(
          alignment: Alignment.topCenter,
          constraints: BoxConstraints(maxWidth: 768),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  S.of(context).hello + greeting + '!',
                  style: Theme.of(context).textTheme.headline4,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  S.of(context).howWouldYouLikeToStart,
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(fontFamily: 'Montserrat'),
                ),
              ),
              Text(S.of(context).manageExercises,
                  style: Theme.of(context).textTheme.headline5),
              AwesomeExpansionTile(
                title: Text(
                    S.of(context).youCanViewExercisesEditThemOrCreateNewOnes),
                leading: FaIcon(FontAwesomeIcons.graduationCap),
                children: [
                  Wrap(
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S.of(context).createNewExercises,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .ifYouNeedToCreateASingleExerciseYouCan),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).createANewExercise),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              EditExercise())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S.of(context).createNewExercisesInStackMode,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .stackModeIsPerfectIfYouWantToCreateMany),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).createInStackMode),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ExerciseStackEdit())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S
                                  .of(context)
                                  .browseExistingExercisesBySubjectOrTopic,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .exercisesAreOrganizedBySubjectTopicAndGradeGoTo),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).viewExercises),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) => Exercises())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S.of(context).searchForExercises,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .ifYouWantToCheckOutExistingExercisesOrSee),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).searchExercises),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              SearchExercise())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Text(S.of(context).manageClasses,
                  style: Theme.of(context).textTheme.headline5),
              AwesomeExpansionTile(
                title: Text(S
                    .of(context)
                    .classesRepresentYourStudentsInYourDigitalClassroomYouCan),
                leading: FaIcon(FontAwesomeIcons.users),
                children: [
                  Wrap(
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S.of(context).viewYourClassesScoreOrEditThem,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .seeYourStudentsScoresByTopicAndGetTheirSubmissions),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).classes),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) => Classes())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Text(S.of(context).examAndControl,
                  style: Theme.of(context).textTheme.headline5),
              AwesomeExpansionTile(
                title: Text(
                    S.of(context).ifYouWantToWriteClassPapersOrAssignHomework),
                leading: FaIcon(FontAwesomeIcons.chalkboardTeacher),
                children: [
                  Wrap(
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S.of(context).manageExerciseSets,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .hereYouCanCreateDeleteAndViewYourAndYour),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).viewExerciseSets),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ExerciseSets())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S.of(context).assignHomeworkToAClass,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .youCanAssignExerciseSetsToYourClassForA),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).selectExerciseSet),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ExerciseSets())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(maxWidth: kBoxWidth),
                        child: TestAppCard(
                          children: [
                            Text(
                              S.of(context).writeAJoinTest,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Text(S
                                .of(context)
                                .aJoinTestRepresentsACheckupAVocabularyTestOr),
                            ButtonBar(
                              children: [
                                TextButton(
                                  child: Text(S.of(context).selectExerciseSet),
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ExerciseSets())),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ))
    ]);
  }
}
