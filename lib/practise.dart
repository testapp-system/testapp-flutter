import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:katex_flutter/katex_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/searchExercise.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/writeTest.dart';
import 'package:url_launcher/url_launcher.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class Practise extends StatefulWidget {
  Practise({Key key}) : super(key: key);

  @override
  _PractiseState createState() => _PractiseState();
}

class _PractiseState extends State<Practise> {
  var api = globals.api;
  bool _topicsLoaded = false;
  Map _topics = Map();
  var _numExercises = 5;
  var _selectedTopics = [];
  int _pageId = 0;
  var _pageController = PageController(initialPage: 0, keepPage: false);
  ScrollController _scrollController = ScrollController();
  bool _showProgress = false;

  Future<void> loadTopics() async {
    api.call('listTopics', context: context).then((data) {
      List rawTopics = data['response'];
      Map subjects = Map();
      rawTopics.forEach((currentTopic) {
        if (!subjects.keys.contains(currentTopic['subject']))
          subjects[currentTopic['subject']] = [];
        subjects[currentTopic['subject']].add(currentTopic);
      });
      setState(() {
        _topicsLoaded = true;
        _topics = subjects;
      });
    });
  }

  void _numExercisesChanged(changed) {
    setState(() {
      _numExercises = changed;
    });
  }

  Future<void> _startTest() async {
    setState(() {
      _showProgress = true;
    });
    Map<String, dynamic> options = Map();
    options['topics'] = _selectedTopics;
    options['number'] = _numExercises;
    options['images'] = true;
    api
        .call('fetchExercises', options: options, context: context)
        .then(((data) {
      setState(() {
        _showProgress = false;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => WriteTest(exercises: data['response'])),
      );
    }));
  }

  List<Map<String, dynamic>> _topicData(context) => [
        {"label": S.of(context).shortTest5Exercises, "number": 5},
        {"label": S.of(context).smartTest10Exercises, "number": 10},
        {"label": S.of(context).bigTest20Exercises, "number": 20},
        {"label": S.of(context).imenseTest50Exercises, "number": 50},
      ];

  @override
  Widget build(BuildContext context) {
    var topicList;
    if (_topicsLoaded) {
      topicList = Expanded(
        child: TestAppScrollBar(
          controller: _scrollController,
          child: ListView.separated(
            controller: _scrollController,
            itemCount: _topics.keys.length,
            itemBuilder: (BuildContext context, int subjectIndex) {
              String subjectName = _topics.keys.toList()[subjectIndex];
              List<Widget> topicTiles = [];
              _topics[subjectName].forEach((topic) {
                topicTiles.add(CheckboxListTile(
                    title: Text(S.of(context).g +
                        ' ' +
                        topic['year'] +
                        ': ' +
                        topic["name"]),
                    value: _selectedTopics.contains(topic["id"]),
                    onChanged: (bool value) {
                      setState(() {
                        if (value) {
                          _selectedTopics.add(topic["id"]);
                        } else {
                          _selectedTopics.remove(topic["id"]);
                        }
                      });
                    }));
              });
              topicTiles.sort((a, b) => ((a as CheckboxListTile).title as Text)
                  .data
                  .compareTo(((a as CheckboxListTile).title as Text).data));

              return (AwesomeExpansionTile(
                title: Text(subjectName),
                //backgroundColor: colorByText(subjectName),
                children: topicTiles,
              ));
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
          ),
        ),
      );
    } else {
      topicList = CenterProgress();
    }

    var isLastPage = (_pageId != 0);
    var nothingSelected = (_selectedTopics.length == 0);
    var fab = (!_showProgress)
        ? FloatingActionButton.extended(
            onPressed: () {
              if (!nothingSelected) {
                isLastPage
                    ? _startTest()
                    : _pageController.nextPage(
                        duration: Duration(milliseconds: 600),
                        curve: Curves.easeInOut);
              }
            },
            backgroundColor:
                nothingSelected ? Colors.grey : Theme.of(context).accentColor,
            label: Row(
              children: <Widget>[
                Text(nothingSelected
                    ? S.of(context).pleaseSelect
                    : isLastPage
                        ? S.of(context).startTest
                        : S.of(context).next),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: FaIcon(isLastPage
                      ? FontAwesomeIcons.caretRight
                      : FontAwesomeIcons.angleRight),
                )
              ],
            ))
        : FloatingActionButton(
            onPressed: () {},
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.white),
            ),
            tooltip: S.of(context).loadingTest,
          );

    return ResponsiveDrawerScaffold(
      extendedBody: true,
      helpPage: HelpPage(
          path: HelpPagePath.randomTest,
          label: S.of(context).helpWithRnadomTests),
      bottomNavigationBar: BottomAppBar(
        shape: kIsWeb
            ? null
            : AutomaticNotchedShape(
                RoundedRectangleBorder(),
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(32)))),
        notchMargin: 8,
        clipBehavior: Clip.antiAlias,
        child: BottomNavigationBar(
          backgroundColor: Theme.of(context).accentColor,
          selectedItemColor: Theme.of(context).cardColor,
          onTap: (selected) {
            if (selected == 1)
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => SearchExercise()));
          },
          items: [
            BottomNavigationBarItem(
                icon: FaIcon(FontAwesomeIcons.folderOpen),
                label: S.of(context).topics),
            BottomNavigationBarItem(
                icon: FaIcon(FontAwesomeIcons.search),
                label: S.of(context).search)
          ],
        ),
      ),
      body: PageView(
        controller: _pageController,
        onPageChanged: (pageId) {
          setState(() {
            _pageId = pageId;
          });
        },
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(
                S.of(context).topics,
                style: Theme.of(context).textTheme.headline5,
              ),
              topicList
            ],
          ),
          Column(
            children: <Widget>[
              //chart,
              Text(
                S.of(context).testType,
                style: Theme.of(context).textTheme.headline5,
              ),
              if (!kIsWeb) DesktopLaTeXTest(),
              Expanded(
                  child: ListView.separated(
                itemCount: _topicData(context).length,
                itemBuilder: (BuildContext context, int index) {
                  return RadioListTile(
                      title: Text(_topicData(context)[index]["label"]),
                      value: _topicData(context)[index]["number"],
                      groupValue: _numExercises,
                      onChanged: _numExercisesChanged);
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              )),
            ],
          ),
        ],
      ),
      floatingActionButton: fab,
      floatingActionButtonLocation: MediaQuery.of(context).size.width > 360
          ? FloatingActionButtonLocation.centerDocked
          : FloatingActionButtonLocation.centerFloat,
    );
  }

  @override
  void initState() {
    loadTopics();
    super.initState();
  }
}

class DesktopLaTeXTest extends StatefulWidget {
  @override
  _DesktopLaTeXTestState createState() => _DesktopLaTeXTestState();
}

class _DesktopLaTeXTestState extends State<DesktopLaTeXTest> {
  bool errorsDetected = false;
  bool laTeXChecked = false;
  String errorMessage = '';

  @override
  Widget build(BuildContext context) {
    if (!laTeXChecked) {
      return Container(
          width: 0,
          height: 0,
          child: (KaTeX(
            laTeXCode: Text('\alpha'),
            onError: (e) {
              try {
                setState(() {
                  laTeXChecked = true;
                  errorsDetected = true;
                  errorMessage = e.toString();
                });
              } catch (e) {
                errorsDetected = true;
              }
            },
          )));
    }
    return (errorsDetected)
        ? Column(
            children: <Widget>[
              Divider(),
              AwesomeExpansionTile(
                leading: FaIcon(
                  Icons.warning,
                  color: Colors.deepOrange,
                  semanticLabel: 'Warning',
                ),
                title: Text('Dependencies missing'),
                subtitle: Text(
                    'Scientific expressions such as Mathematical equations or Physical symbols may be displayed incorrectly. Tap to learn more.'),
                children: <Widget>[
                  ListTile(
                    title: Text(
                        'On Fuchsia, Linux, macOS and Windows, TestApp requires the following libraries installed. Ensure they are available in PATH.'),
                  ),
                  ListTile(
                    title: Text(
                      'LaTeX',
                    ),
                    onTap: showLaTeXDialog,
                  ),
                  ListTile(
                    title: Text(
                      'ImageMagick',
                    ),
                    onTap: showImageMagickDialog,
                  ),
                ],
              ),
              Divider(),
            ],
          )
        : Container();
  }

  void showImageMagickDialog() => showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('Install ImagMagick'),
            content: Container(
              width: double.maxFinite,
              //constraints: BoxConstraints(maxWidth: 320),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  AwesomeExpansionTile(
                    title: Text('Windows'),
                    leading: FaIcon(FontAwesomeIcons.windows),
                    children: <Widget>[
                      ListTile(
                        title:
                            Text('Download the official ImageMagick installer'),
                      ),
                      ListTile(
                        title: SelectableText(
                            'https://imagemagick.org/script/download.php#windows'),
                        onTap: () => launch(
                            'https://imagemagick.org/script/download.php#windows'),
                      ),
                      ListTile(
                        title: Text('Execute the installer'),
                      ),
                      ListTile(
                        title: Text('Re-open TestApp'),
                      ),
                    ],
                  ),
                  AwesomeExpansionTile(
                    title: Text('macOS'),
                    leading: FaIcon(FontAwesomeIcons.apple),
                    children: <Widget>[
                      ListTile(
                        title: Text('Open the Terminal app'),
                      ),
                      ListTile(
                        title: Text('Run the following two commands:'),
                      ),
                      Divider(),
                      ListTile(
                        title: SelectableText('brew install imagemagick'),
                      ),
                      ListTile(
                        title: SelectableText('brew install ghostscript'),
                      ),
                      Divider(),
                      ListTile(
                        title: Text('Re-open TestApp'),
                      ),
                    ],
                  ),
                  AwesomeExpansionTile(
                    title: Text('Linux, Unix and Fuchsia'),
                    leading: FaIcon(FontAwesomeIcons.linux),
                    children: <Widget>[
                      ListTile(
                        title: Text(
                            'Install the imagemagick package for your distribution'),
                      ),
                      ListTile(
                        title: Text('Re-open TestApp'),
                      ),
                    ],
                  )
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: Navigator.of(context).pop, child: Text('Ok'))
            ],
          ));

  void showLaTeXDialog() => showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('Install LaTeX'),
            content: Container(
              width: double.maxFinite,
              constraints: BoxConstraints(maxWidth: 320),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  AwesomeExpansionTile(
                    title: Text('Windows'),
                    leading: FaIcon(FontAwesomeIcons.windows),
                    children: <Widget>[
                      ListTile(
                        title: Text('Download the official MikTeX installer'),
                        subtitle: Text(
                            'Note: We alternatively support TeX Live too.'),
                      ),
                      ListTile(
                        title: SelectableText('https://miktex.org/download'),
                        onTap: () => launch('https://miktex.org/download'),
                      ),
                      ListTile(
                        title: Text('Execute the installer'),
                      ),
                      ListTile(
                        title: Text('Reboot the computer'),
                      ),
                    ],
                  ),
                  AwesomeExpansionTile(
                    title: Text('macOS'),
                    leading: FaIcon(FontAwesomeIcons.apple),
                    children: <Widget>[
                      ListTile(
                        title: Text('Download the official MikTeX installer'),
                        subtitle: Text(
                            'Note: We alternatively support TeX Live too.'),
                      ),
                      ListTile(
                        title: SelectableText('https://miktex.org/download'),
                        onTap: () => launch('https://miktex.org/download'),
                      ),
                      ListTile(
                        title: Text(
                            'Open the installer and drag MikTeX to Applications'),
                      ),
                      ListTile(
                        title: Text(
                            'Follow these instructions to add MikTeX to PATH'),
                        onTap: () =>
                            launch('https://miktex.org/howto/modify-path'),
                      ),
                      ListTile(
                        title: Text('Reboot your Mac'),
                      ),
                    ],
                  ),
                  AwesomeExpansionTile(
                    title: Text('Linux, Unix and Fuchsia'),
                    leading: FaIcon(FontAwesomeIcons.linux),
                    children: <Widget>[
                      ListTile(
                        title: Text(
                            'Install texlive, texlive-base, texlive-bin, texlive-most or miktex for your distribution'),
                      ),
                      ListTile(
                        title: Text('Re-open TestApp'),
                      ),
                    ],
                  )
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: Navigator.of(context).pop, child: Text('Ok'))
            ],
          ));
}
