import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'generated/l10n.dart';

class TestAppCard extends StatelessWidget {
  TestAppCard({Key key, this.children}) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        constraints: BoxConstraints(maxWidth: 720),
        width: double.infinity,
        child: (Card(
          child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: children,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
              )),
        )),
      ),
    );
  }
}

Color colorByText(text) {
  var rgbString = text.hashCode.toString().substring(0, 6);
  return Color.fromRGBO(
      int.parse(rgbString.substring(0, 2)),
      int.parse(rgbString.substring(2, 4)),
      int.parse(rgbString.substring(4, 6)),
      1);
}

class RandomBear extends StatelessWidget {
  final String type;
  final int offset;
  final double scale;

  RandomBear({this.type = 'joky', this.offset, this.scale = 1});

  @override
  Widget build(BuildContext context) {
    String location = 'assets/bears/bear';
    if (['bad', 'good', 'joky', 'super'].contains(this.type)) {
      location += '-' + this.type;
      Map offsetSizes = {'bad': 6, 'joky': 14, 'good': 7, 'super': 2};
      if (this.offset != null && this.offset <= offsetSizes[this.type]) {
        location += '-' + this.offset.toString() + '.gif';
      } else {
        location += '-' +
            new Random().nextInt(offsetSizes[this.type]).toString() +
            '.gif';
      }
    } else {
      location += '-standard.png';
    }
    return Image.asset(
      location,
      excludeFromSemantics: true,
      scale: this.scale,
    );
  }
}

class CenterProgress extends StatefulWidget {
  final String label;

  CenterProgress({this.label = ''});

  @override
  _CenterProgressState createState() => _CenterProgressState();
}

class _CenterProgressState extends State<CenterProgress>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<Color> _colorTween;
  TweenSequence<Color> _tweenSequence = TweenSequence([
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.green, end: Colors.lightBlue),
        weight: 1),
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.lightBlue, end: Colors.lightBlue),
        weight: 2),
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.lightBlue, end: Colors.green),
        weight: 1),
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.green, end: Colors.green), weight: 2),
  ]);

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 4));
    _colorTween = _tweenSequence.animate(_animationController);
    _animationController.repeat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _colorTween,
        builder: (context, child) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 48,
                    height: 48,
                    child: CircularProgressIndicator(
                      semanticsLabel: S.of(context).loadingData,
                      valueColor: _colorTween,
                    ),
                  ),
                  if (widget.label != '')
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(widget.label),
                    ),
                ],
              ),
            )));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}

//// EXPANSION TILE

// Copyright 2014 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

const Duration _kExpand = Duration(milliseconds: 200);

/// A single-line [ListTile] with a trailing button that expands or collapses
/// the tile to reveal or hide the [children].
///
/// This widget is typically used with [ListView] to create an
/// "expand / collapse" list entry. When used with scrolling widgets like
/// [ListView], a unique [PageStorageKey] must be specified to enable the
/// [AwesomeExpansionTile] to save and restore its expanded state when it is scrolled
/// in and out of view.
///
/// See also:
///
///  * [ListTile], useful for creating expansion tile [children] when the
///    expansion tile represents a sublist.
///  * The "Expand/collapse" section of
///    <https://material.io/guidelines/components/lists-controls.html>.
class AwesomeExpansionTile extends StatefulWidget {
  /// Creates a single-line [ListTile] with a trailing button that expands or collapses
  /// the tile to reveal or hide the [children]. The [initiallyExpanded] property must
  /// be non-null.
  const AwesomeExpansionTile({
    Key key,
    this.leading,
    @required this.title,
    this.subtitle,
    this.backgroundColor,
    this.onExpansionChanged,
    this.children = const <Widget>[],
    this.trailing,
    this.initiallyExpanded = false,
  })  : assert(initiallyExpanded != null),
        super(key: key);

  /// A widget to display before the title.
  ///
  /// Typically a [CircleAvatar] widget.
  final Widget leading;

  /// The primary content of the list item.
  ///
  /// Typically a [Text] widget.
  final Widget title;

  /// Additional content displayed below the title.
  ///
  /// Typically a [Text] widget.
  final Widget subtitle;

  /// Called when the tile expands or collapses.
  ///
  /// When the tile starts expanding, this function is called with the value
  /// true. When the tile starts collapsing, this function is called with
  /// the value false.
  final ValueChanged<bool> onExpansionChanged;

  /// The widgets that are displayed when the tile expands.
  ///
  /// Typically [ListTile] widgets.
  final List<Widget> children;

  /// The color to display behind the sublist when expanded.
  final Color backgroundColor;

  /// A widget to display instead of a rotating arrow icon.
  final Widget trailing;

  /// Specifies if the list tile is initially expanded (true) or collapsed (false, the default).
  final bool initiallyExpanded;

  @override
  _AwesomeExpansionTileState createState() => _AwesomeExpansionTileState();
}

class _AwesomeExpansionTileState extends State<AwesomeExpansionTile>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeOutTween =
      CurveTween(curve: Curves.easeOut);
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);
  static final Animatable<double> _halfTween =
      Tween<double>(begin: 0.0, end: 0.5);

  final ColorTween _borderColorTween = ColorTween();
  final ColorTween _headerColorTween = ColorTween();
  final ColorTween _iconColorTween = ColorTween();
  final ColorTween _backgroundColorTween = ColorTween();

  AnimationController _controller;
  Animation<double> _iconTurns;
  Animation<double> _heightFactor;

  //Animation<Color> _borderColor;
  Animation<Color> _headerColor;
  Animation<Color> _iconColor;
  Animation<Color> _backgroundColor;

  bool _isExpanded = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _kExpand, vsync: this);
    _heightFactor = _controller.drive(_easeInTween);
    _iconTurns = _controller.drive(_halfTween.chain(_easeInTween));
    //_borderColor = _controller.drive(_borderColorTween.chain(_easeOutTween));
    _headerColor = _controller.drive(_headerColorTween.chain(_easeInTween));
    _iconColor = _controller.drive(_iconColorTween.chain(_easeInTween));
    _backgroundColor =
        _controller.drive(_backgroundColorTween.chain(_easeOutTween));

    _isExpanded = PageStorage.of(context)?.readState(context) as bool ??
        widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _handleTap() {
    setState(() {
      _isExpanded = !_isExpanded;
      if (_isExpanded) {
        _controller.forward();
      } else {
        _controller.reverse().then<void>((void value) {
          if (!mounted) return;
          setState(() {
            // Rebuild without widget.children.
          });
        });
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
    if (widget.onExpansionChanged != null)
      widget.onExpansionChanged(_isExpanded);
  }

  Widget _buildChildren(BuildContext context, Widget child) {
    //final Color borderSideColor = _borderColor.value ?? Colors.transparent;

    return Container(
      decoration: BoxDecoration(
        color: _backgroundColor.value ?? Colors.transparent,
        /*border: Border(
          top: BorderSide(color: borderSideColor),
          bottom: BorderSide(color: borderSideColor),
        ),*/
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTileTheme.merge(
            iconColor: _iconColor.value,
            textColor: _headerColor.value,
            child: ListTile(
              onTap: _handleTap,
              leading: widget.leading,
              title: widget.title,
              subtitle: widget.subtitle,
              trailing: widget.trailing ??
                  RotationTransition(
                    turns: _iconTurns,
                    child: const FaIcon(FontAwesomeIcons.angleDown),
                  ),
            ),
          ),
          ClipRect(
            child: Align(
              heightFactor: _heightFactor.value,
              child: child,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void didChangeDependencies() {
    final ThemeData theme = Theme.of(context);
    _borderColorTween.end = theme.dividerColor;
    _headerColorTween
      ..begin = theme.textTheme.subtitle1.color
      ..end = theme.accentColor;
    _iconColorTween
      ..begin = theme.unselectedWidgetColor
      ..end = theme.accentColor;
    _backgroundColorTween.end = widget.backgroundColor;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final bool closed = !_isExpanded && _controller.isDismissed;
    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: closed ? null : Column(children: widget.children),
    );
  }
}

class TestAppScrollBar extends StatelessWidget {
  final Widget child;
  final ScrollController controller;

  TestAppScrollBar({@required this.child, @required this.controller});

  @override
  Widget build(BuildContext context) {
    bool isDesktop = (kIsWeb ||
        (Theme.of(context).platform != TargetPlatform.iOS &&
            Theme.of(context).platform != TargetPlatform.android));
    return (isDesktop
        ? CupertinoScrollbar(
            child: child,
            isAlwaysShown: true,
            controller: controller,
          )
        : Scrollbar(
            child: child,
            controller: controller,
          ));
  }
}
