import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:package_info/package_info.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/gdpr.dart';
import 'package:test_app_flutter/style.dart';
import 'package:url_launcher/url_launcher.dart';

import 'generated/l10n.dart';

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  ScrollController _scrollController = ScrollController();
  PackageInfo info;

  @override
  void initState() {
    super.initState();
    PackageInfo.fromPlatform().then((value) => setState(() => info = value));
  }

  @override
  Widget build(BuildContext context) {
    bool _isIOS = Theme.of(context).platform == TargetPlatform.iOS;
    List platforms = [
      {
        'icon': FontAwesomeIcons.android,
        'name': 'Android',
        'link':
            'http://play.google.com/store/apps/details?id=ga.testapp.testapp',
      },
      {
        'icon': FontAwesomeIcons.windows,
        'name': 'Windows 10',
        'link': 'https://www.microsoft.com/store/productId/9P9JJFMFF77D',
      },
      {
        'icon': FontAwesomeIcons.chrome,
        'name': 'Chrome',
        'link':
            'https://chrome.google.com/webstore/detail/testapp-system/hclopnbfffconajgdcmibjekjhmfegjf',
      },
      {
        'icon': FontAwesomeIcons.linux,
        'name': 'Linux',
        'link': 'https://snapcraft.io/testapp-desktop',
      },
      {
        'icon': FontAwesomeIcons.apple,
        'name': 'iOS',
        'link': 'https://apps.apple.com/us/app/testapp-system/id1490425513',
      },
    ];
    return ResponsiveDrawerScaffold(
        helpPage: HelpPage(path: HelpPagePath.about),
        title: S.of(context).aboutTestapp,
        body: TestAppScrollBar(
          controller: _scrollController,
          child: ListView(controller: _scrollController, children: [
            TestAppCard(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    S.of(context).about,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(S
                      .of(context)
                      .testappIsAnEducationalOpensourceProjectForWritingClassTests),
                ),
                Image.asset(
                  S.of(context).assetsgooglePlayEnpng,
                  alignment: Alignment.center,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    S
                        .of(context)
                        .useOfTestappIsFreeForOrganizationsConditionsApplyntheSource,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RichText(
                      text: TextSpan(
                    style: Theme.of(context).textTheme.bodyText2,
                    children: [
                      TextSpan(
                        text: S.of(context).chaw,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                          text: S
                              .of(context)
                              .niqhomLoJiqochbeQuvJilijJijatlhmoTlhinganHol)
                    ],
                  )),
                ),
                ButtonBar(
                  children: <Widget>[
                    OutlinedButton.icon(
                      label: Text(S.of(context).website),
                      icon: FaIcon(FontAwesomeIcons.globe),
                      onPressed: () {
                        launch(S.of(context).testAppWebSite);
                      },
                    ),
                    OutlinedButton.icon(
                      label: Text('GitLab'),
                      icon: FaIcon(FontAwesomeIcons.gitlab),
                      onPressed: () {
                        launch('https://gitlab.com/testapp-system/');
                      },
                    ),
                    OutlinedButton.icon(
                      label: Text('EUPL-1.2'),
                      icon: FaIcon(FontAwesomeIcons.balanceScaleRight),
                      onPressed: () {
                        launch(S.of(context).euplUrl);
                      },
                    ),
                    OutlinedButton.icon(
                      label: Text('Details'),
                      icon: FaIcon(FontAwesomeIcons.history),
                      onPressed: () {
                        showLicensePage(
                          context: context,
                          applicationName: 'TestApp',
                          applicationVersion:
                              'Version ${info?.version} build ${info?.buildNumber}' ??
                                  S.of(context).unknown,
                          applicationIcon:
                              Image.asset('assets/launcher-web.png', scale: 8),
                          applicationLegalese:
                              S.of(context).aProductOfTestappschule,
                          /*children: [
                            Image.asset(S.of(context).assetsgooglePlayEnpng,
                                scale: 2),
                            if (!_isIOS)
                              ElevatedButton.icon(
                                  onPressed: () =>
                                      launch('https://buymeacoff.ee/braid'),
                                  icon: FaIcon(FontAwesomeIcons.coffee),
                                  label: Text('Buy us a cup of tea')),
                          ],*/
                        );
                      },
                    )
                  ],
                )
              ],
            ),
            if (!_isIOS)
              TestAppCard(
                children: <Widget>[
                  Text(
                    S.of(context).testappForAllDevices,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: platforms.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                      itemBuilder: (context, index) {
                        return (ListTile(
                          onTap: () {
                            launch(platforms[index]['link']);
                          },
                          leading: FaIcon(platforms[index]['icon']),
                          title: Text(platforms[index]['name']),
                        ));
                      })
                ],
              ),
            if (_isIOS)
              TestAppCard(
                children: <Widget>[
                  Text(
                    S.of(context).learnMore,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  ListTile(
                      onTap: () {
                        String langCode =
                            (Localizations.localeOf(context).languageCode + '_')
                                .substring(0, 3)
                                .replaceAll('_', '');
                        launch(Uri.encodeFull(
                            'https://docs.testapp.ga/' + langCode + '/'));
                      },
                      leading: FaIcon(FontAwesomeIcons.globe),
                      title: Text(S
                          .of(context)
                          .ifYouWantToLearnMoreAboutTestappHelpOur))
                ],
              ),
            TestAppCard(children: [
              Text(
                S.of(context).legalNoticeAndPrivacyPolicy,
                style: Theme.of(context).textTheme.headline6,
              ),
              Text(S
                  .of(context)
                  .asThisApplicationIsANoncommercialStudentProjectByA),
              GDPR()
            ]),
          ]),
        ));
  }
}
