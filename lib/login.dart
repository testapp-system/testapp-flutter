import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/api.dart';
import 'package:test_app_flutter/dashboard.dart';
import 'package:test_app_flutter/gdpr.dart';
import 'package:test_app_flutter/slpashScreen.dart';
import 'package:test_app_flutter/style.dart';
import 'package:unicorndial/unicorndial.dart';
import 'package:url_launcher/url_launcher.dart';

import 'drawer.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with WidgetsBindingObserver {
  var api = globals.api;
  final loginNameField = TextEditingController();
  final loginMailField = TextEditingController();
  final loginPassField = TextEditingController();
  final loginPassField2 = TextEditingController();
  final loginVoucherField = TextEditingController();
  bool isAdmin = false;
  int selectedSchool = 0;
  List schoolList = [
    {'id': '0', 'name': 'Loading...'}
  ];

  bool _showFullScreenLoading = false;
  bool _checkingForExistingAccount = false;
  bool _stayLoggedIn = true;
  bool _showWrongLogin = false;
  bool _showFillAllFields = false;

  //Login UI
  int _currentPage = 0;

  TextEditingController _customServerController =
      TextEditingController(text: 'testapp.ga');

  ScrollController _scrollController = ScrollController();

  void _onItemTapped(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  Future login() async {
    setState(() {
      _showFullScreenLoading = true;
    });
    return await api
        .call('login',
            options: {
              'mail': loginMailField.text.trim().toLowerCase(),
              'pass': loginPassField.text,
              'stay': _stayLoggedIn
            },
            context: context)
        .then((data) async {
      _showFullScreenLoading = false;
      if (data['response'] == true) {
        _showWrongLogin = false;
        await adminCheck(context);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => Dashboard()),
        );
      } else {
        _showWrongLogin = true;
      }
      setState(() {});
    });
  }

  void register({teacher = false}) {
    if (loginMailField.text.trim().isEmpty ||
        loginPassField.text.isEmpty ||
        loginNameField.text.isEmpty ||
        loginPassField.text != loginPassField2.text ||
        (!loginNameField.text
            .contains(RegExp('[a-zA-ZäÄöÖüÜéè]+ [a-zA-ZäÄöÖüÜéè]+'))) ||
        (!loginMailField.text.trim().contains(RegExp(
            // General Email Regex (RFC 5322 Official Standard)
            '^(?:[a-z0-9!#\$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&\'*+/=?^_`{|}~-]+)*|"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])\$')))) {
      setState(() {
        _showFillAllFields = true;
      });
      return;
    } else {
      setState(() {
        _showFillAllFields = false;
      });
    }
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(
                S.of(context).privacyIsImportantToUs,
              ),
              content: Container(
                width: double.maxFinite,
                constraints: BoxConstraints(maxWidth: 320),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Image.asset('assets/image-privacy.png',
                        scale:
                            (MediaQuery.of(context).size.width < 768) ? 1 : 2),
                    SelectableText.rich(
                        TextSpan(text: S.of(context).privacyPolicyShort)),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: Text(S.of(context).readFullPrivacyPolicy),
                  onPressed: () => showSimplifiedGDPR(context),
                ),
                TextButton(
                  child: Text(
                      S.of(context).iReadTheFullPrivacyStatementAndAgreeToAll),
                  onPressed: () async {
                    Map<String, dynamic> options = {
                      'mail': loginMailField.text.trim().toLowerCase(),
                      'pass': loginPassField.text,
                      'school': selectedSchool,
                      'name': loginNameField.text.trim(),
                    };
                    if (teacher) {
                      options['accesslevel'] = 1;
                      options['voucher'] =
                          loginVoucherField.text.trim().toUpperCase();
                    }
                    setState(() {
                      _showFullScreenLoading = true;
                    });
                    api
                        .call('register', options: options, context: context)
                        .then((data) {
                      print(data);
                      setState(() {
                        _showFullScreenLoading = false;
                      });
                      login().then((data) {
                        _checkingForExistingAccount = false;
                      });
                      if (data['response'] != true) {
                        _checkingForExistingAccount = true;
                      }
                    });
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  void passwordReset() {
    showDialog(
        context: context,
        builder: (context) => Dialog(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(S.of(context).resetPassword,
                        style: Theme.of(context).textTheme.headline5),
                    Text(S.of(context).areYouSureToResetThePasswordFor +
                        loginMailField.text.toLowerCase().trim() +
                        '?'),
                    ButtonBar(
                      children: <Widget>[
                        MaterialButton(
                          child: Text(S.of(context).cancel),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        OutlinedButton(
                          child: Text(S.of(context).reset),
                          onPressed: () async {
                            api
                                .call('passwordReset',
                                    options: {
                                      'mail': loginMailField.text
                                          .toLowerCase()
                                          .trim()
                                    },
                                    context: context)
                                .then((data) {
                              Navigator.pop(context);
                              showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                        content: Text(
                                            S.of(context).pleaseCheckYourMails),
                                      ));
                            });
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
            ));
  }

  @override
  void initState() {
    _customServerController.addListener(_handleCustomServerChange);
    _listSchools();
    api.call('loginState', context: context).then((data) {
      if (data["response"] == true) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Dashboard()),
        );
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _customServerController.removeListener(_handleCustomServerChange);
    _customServerController.dispose();
    loginMailField.dispose();
    loginNameField.dispose();
    loginPassField.dispose();
    loginVoucherField.dispose();
    super.dispose();
  }

  @override
  void didChangePlatformBrightness() {
    print(WidgetsBinding.instance.window
        .platformBrightness); // should print Brightness.light / Brightness.dark when you switch
    super.didChangePlatformBrightness(); // make sure you call this
  }

  @override
  Widget build(BuildContext context) {
    Widget page;
    Widget schoolDropdown = Row(children: [
      Text(S.of(context).school),
      DropdownButton<int>(
        value: selectedSchool,
        icon: FaIcon(FontAwesomeIcons.school),
        iconSize: 24,
        elevation: 16,
        //itemHeight: 48,
        style: TextStyle(color: Colors.lightBlue),
        underline: Container(
          height: 2,
          color: Colors.green,
        ),
        onChanged: (int newValue) {
          setState(() {
            selectedSchool = newValue;
          });
        },
        items: schoolList.map((school) {
          return DropdownMenuItem<int>(
            value: int.parse(school['id']),
            child: Text(
              school['name'],
              style: Theme.of(context).textTheme.bodyText1,
            ),
          );
        }).toList(),
      )
    ]);
    switch (_currentPage) {
      case 0: //Login
        page = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              S.of(context).loginAlStudentOrTeacher,
              style: Theme.of(context).textTheme.headline5,
            ),
            TextField(
              controller: loginMailField,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).mailId),
            ),
            TextField(
              controller: loginPassField,
              obscureText: true,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).password),
            ),
            if (_showWrongLogin)
              Text(S.of(context).wrongMailIdOrPassword,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: Colors.red)),
            if (kIsWeb)
              Row(
                children: <Widget>[
                  Text(S.of(context).stayLoggedIn),
                  Checkbox(
                      value: _stayLoggedIn,
                      onChanged: (checked) =>
                          setState(() => _stayLoggedIn = checked))
                ],
              ),
            ButtonBar(
              children: <Widget>[
                MaterialButton(
                  onPressed: passwordReset,
                  child: Text(S.of(context).resetPassword),
                ),
                ElevatedButton(
                  onPressed: login,
                  child: Text(S.of(context).login),
                ),
              ],
            )
          ],
        );
        break;
      case 1: //Register
        page = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              S.of(context).register,
              style: Theme.of(context).textTheme.headline5,
            ),
            TextField(
              controller: loginNameField,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  helperText: S.of(context).thisIsImportantForYourTeachers,
                  labelText: S.of(context).fullName),
            ),
            TextField(
              controller: loginMailField,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).mailId),
            ),
            TextField(
              controller: loginPassField,
              obscureText: true,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).password),
            ),
            TextField(
              controller: loginPassField2,
              obscureText: true,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).repeatPassword),
            ),
            schoolDropdown,
            if (kIsWeb)
              Row(
                children: <Widget>[
                  Text(S.of(context).stayLoggedIn),
                  Checkbox(
                      value: _stayLoggedIn,
                      onChanged: (checked) =>
                          setState(() => _stayLoggedIn = checked))
                ],
              ),
            if (_showFillAllFields)
              Text(S.of(context).pleaseProvideAFullNameAValidMailIdAnd,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: Colors.red)),
            ButtonBar(
              children: <Widget>[
                ElevatedButton(
                  onPressed: register,
                  child: Text(S.of(context).register),
                ),
              ],
            )
          ],
        );
        break;
      case 2: //Teacher Register
        page = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              S.of(context).teacherRegister,
              style: Theme.of(context).textTheme.headline5,
            ),
            Text(S.of(context).toPreventAbuseOfTeacherAccountsYouNeedToInsert),
            TextField(
              controller: loginNameField,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  helperText: S.of(context).thisIsImportantForYourStudents,
                  labelText: S.of(context).fullName),
            ),
            TextField(
              controller: loginMailField,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).mailId),
            ),
            TextField(
              controller: loginPassField,
              obscureText: true,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).password),
            ),
            TextField(
              controller: loginPassField2,
              obscureText: true,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).repeatPassword),
            ),
            TextField(
              controller: loginVoucherField,
              obscureText: true,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: S.of(context).accessVoucher,
                  helperText: S
                      .of(context)
                      .ifYouDidntGetAVoucherPleaseContactYourSchools),
            ),
            schoolDropdown,
            if (kIsWeb)
              Row(
                children: <Widget>[
                  Text(S.of(context).stayLoggedIn),
                  Checkbox(
                      value: _stayLoggedIn,
                      onChanged: (checked) =>
                          setState(() => _stayLoggedIn = checked))
                ],
              ),
            if (_showFillAllFields)
              Text(S.of(context).pleaseProvideAFullNameAValidMailIdAnd,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: Colors.red)),
            ButtonBar(
              children: <Widget>[
                ElevatedButton(
                  onPressed: () {
                    register(teacher: true);
                  },
                  child: Text(S.of(context).register),
                ),
              ],
            )
          ],
        );
        break;
    }
    return (!_showFullScreenLoading)
        ? Scaffold(
            body: SplashBackground(
              child: TestAppScrollBar(
                controller: _scrollController,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 32, 8, 8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Center(
                              child: Container(
                            constraints: BoxConstraints(maxWidth: 768),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12))),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        ElevatedButton.icon(
                                            style: ElevatedButton.styleFrom(
                                              primary: Theme.of(context)
                                                  .dialogBackgroundColor, // background
                                              onPrimary: Colors.white,
                                              elevation: 8,
                                              padding: EdgeInsets.all(12),
                                            ),
                                            onPressed: () => launch(
                                                S.of(context).testAppWebSite),
                                            icon: FaIcon(
                                              FontAwesomeIcons.solidStar,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                            label: Text(
                                              S.of(context).newToTestapp,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5,
                                            )),
                                        IconTheme(
                                          data: Theme.of(context)
                                              .iconTheme
                                              .copyWith(
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                          child: HelpPopup(
                                            helpPage: HelpPage(
                                                path: HelpPagePath.login,
                                                label: S
                                                    .of(context)
                                                    .helpGettingStarted),
                                            showGdpr: true,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      'TestApp',
                                      style:
                                          Theme.of(context).textTheme.headline1,
                                    ),
                                    Hero(
                                      tag: RandomBear,
                                      child: RandomBear(
                                        type: 'joky',
                                        offset: 12,
                                        scale: 1.5,
                                      ),
                                    ),
                                    page,
                                  ],
                                ),
                              ),
                            ),
                          )),
                          if (!kIsWeb)
                            AwesomeExpansionTile(
                              backgroundColor: Theme.of(context).cardColor,
                              title: Text(S.of(context).connectToOtherServer),
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: TextField(
                                    controller: _customServerController,
                                    decoration: InputDecoration(
                                        labelText: S.of(context).testappServer,
                                        helperText: S
                                            .of(context)
                                            .egMyservercomOrHttpsdevtestappga),
                                  ),
                                )
                              ],
                            )
                        ],
                      )),
                ),
              ),
            ),
            floatingActionButton: UnicornDialer(
              parentHeroTag: 'language',
              parentButton: Icon(FontAwesomeIcons.globe),
              finalButtonIcon: Icon(FontAwesomeIcons.times),
              childButtons: [
                UnicornButton(
                  currentButton: FloatingActionButton(
                    heroTag: 'en',
                    tooltip: 'English',
                    onPressed: () => setState(() {
                      S.load(Locale('en', 'EU'));
                      Preferences().save('language', 'en');
                    }),
                    mini: true,
                    child: Image.asset('assets/languages/europe.png'),
                  ),
                ),
                UnicornButton(
                  currentButton: FloatingActionButton(
                    heroTag: 'de',
                    tooltip: 'Deutsch',
                    onPressed: () => setState(() {
                      S.load(Locale('de'));
                      Preferences().save('language', 'de');
                    }),
                    mini: true,
                    child: Image.asset('assets/languages/germany.png'),
                  ),
                ),
                UnicornButton(
                  currentButton: FloatingActionButton(
                    heroTag: 'fr',
                    tooltip: 'Français',
                    onPressed: () => setState(() {
                      S.load(Locale('fr'));
                      Preferences().save('language', 'fr');
                    }),
                    mini: true,
                    child: Image.asset('assets/languages/france.png'),
                  ),
                ),
                UnicornButton(
                  currentButton: FloatingActionButton(
                    heroTag: 'tlh',
                    backgroundColor: Colors.red,
                    tooltip: 'tlhIngan Hol',
                    onPressed: () => setState(() {
                      S.load(Locale('tlh'));
                      Preferences().save('language', 'tlh');
                    }),
                    mini: true,
                    child: Image.asset('assets/languages/klingon.png'),
                  ),
                ),
              ],
            ),
            bottomNavigationBar: BottomNavigationBar(
              backgroundColor: Colors.lightBlue,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: FaIcon(FontAwesomeIcons.key),
                  label: S.of(context).login,
                ),
                BottomNavigationBarItem(
                  icon: FaIcon(FontAwesomeIcons.userPlus),
                  label: S.of(context).register,
                ),
                BottomNavigationBarItem(
                  icon: FaIcon(FontAwesomeIcons.chalkboardTeacher),
                  label: S.of(context).teacher,
                ),
              ],
              currentIndex: _currentPage,
              unselectedItemColor: Colors.grey[200],
              selectedItemColor: Colors.white,
              onTap: _onItemTapped,
            ),
          )
        : Scaffold(
            body: CenterProgress(
            label: (!_checkingForExistingAccount)
                ? S.of(context).checkingCredentials
                : S
                    .of(context)
                    .couldNotCreateThisAccountLetsSeeWhetherYouAlready,
          ));
  }

  Future<void> _listSchools() async {
    api.call('listSchools', context: context).then((data) {
      setState(() {
        schoolList = data['response'];
      });
    }).catchError((e) {
      // The server seems to be invalid
    });
  }

  void _handleCustomServerChange() {
    globals.api.setBaseUrl(_customServerController.text);
    _listSchools();
  }
}

Future adminCheck(BuildContext context) async {
  return globals.api.call('userInfo', context: context).then((data) async {
    globals.userInfo = data;
    await Preferences().save(
        "Admin",
        (data['response'] != false &&
                int.parse(data['response']['accesslevel']) >= 1)
            .toString());
    globals.isAdmin = (data['response'] != false &&
        int.parse(data['response']['accesslevel']) >= 1);
  });
}
