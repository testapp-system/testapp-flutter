import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/chart.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/exerciseSolution.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/userScore.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class TestScore extends StatefulWidget {
  final int testId;
  final bool backToUserScore;
  final bool hideEasterEgg;

  TestScore(
      {Key key,
      @required this.testId,
      this.backToUserScore = false,
      this.hideEasterEgg = false})
      : super(key: key);

  @override
  _TestScoreState createState() => _TestScoreState();
}

class _TestScoreState extends State<TestScore> with WidgetsBindingObserver {
  Map _testScore = Map();
  bool _testFetched = false;
  ScrollController _scrollController = ScrollController();

  int _exerciseLoading = -1;

  @override
  void didChangePlatformBrightness() {
    super.didChangePlatformBrightness(); // make sure you call this
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
        helpPage: HelpPage(
            path: HelpPagePath.testScore,
            label: S.of(context).wrongResultsneedHelp),
        title: S.of(context).testScore,
        appBarLeading: IconButton(
          icon: FaIcon(FontAwesomeIcons.times),
          onPressed: () => (widget.backToUserScore)
              ? Navigator.of(context)
                  .push(MaterialPageRoute(builder: (b) => UserScore()))
              : Navigator.of(context).pop(),
          tooltip: S.of(context).close,
        ),
        body: (_testFetched)
            ? (_testScore.isNotEmpty)
                ? Container(
                    color: Theme.of(context).cardColor,
                    child: TestAppScrollBar(
                      controller: _scrollController,
                      child: ListView.builder(
                          controller: _scrollController,
                          itemCount: _testScore['answers'].length + 1,
                          itemBuilder: (context, index) {
                            if (index == 0) {
                              return Column(
                                children: <Widget>[
                                  ScoreChart(
                                    100 * _testScore['score'].toDouble(),
                                    labelText: (widget.hideEasterEgg)
                                        ? S.of(context).studentGenitive
                                        : S.of(context).your,
                                  ),
                                  (!widget.hideEasterEgg)
                                      ? RandomBear(
                                          type: (_testScore['score']
                                                      .toDouble() <
                                                  0.65)
                                              ? 'bad'
                                              : (_testScore['score']
                                                              .toDouble() ==
                                                          1.0 &&
                                                      new Random()
                                                              .nextInt(10) ==
                                                          9)
                                                  ? 'super'
                                                  : 'good',
                                          scale: 1.5,
                                        )
                                      : Container(),
                                ],
                              );
                            }
                            var currentAnswer =
                                _testScore['answers'][index - 1];

                            List<Widget> yourAnswerRow = [
                              Text((widget.hideEasterEgg)
                                  ? S.of(context).studentsAnswer
                                  : S.of(context).yourAnswer)
                            ];
                            List<Widget> correctAnswerRow = [
                              Text(S.of(context).correctAnswer)
                            ];
                            jsonDecode(currentAnswer['answer'])
                                .forEach(((awText) {
                              if (awText != null)
                                yourAnswerRow.add(Chip(
                                  label: TestAppTex(
                                    awText,
                                    inheritWidth: false,
                                    background: MediaQuery.of(context)
                                                .platformBrightness ==
                                            Brightness.dark
                                        ? Theme.of(context).cardColor
                                        : Colors.grey.shade300,
                                  ),
                                ));
                            }));
                            jsonDecode(currentAnswer['correct'])
                                .forEach(((awText) {
                              if (awText != null)
                                correctAnswerRow.add(Chip(
                                    label: TestAppTex(
                                  awText,
                                  inheritWidth: false,
                                  background: MediaQuery.of(context)
                                              .platformBrightness ==
                                          Brightness.dark
                                      ? Theme.of(context).cardColor
                                      : Colors.grey.shade300,
                                )));
                            }));

                            return (AwesomeExpansionTile(
                              title: TestAppTex(
                                currentAnswer['question'],
                                inheritWidth: false,
                              ),
                              leading: (currentAnswer['match'])
                                  ? FaIcon(FontAwesomeIcons.checkCircle)
                                  : FaIcon(FontAwesomeIcons.timesCircle),
                              backgroundColor: (currentAnswer['match'])
                                  ? Colors.lightGreen[300]
                                  : Colors.deepOrange[300],
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: IntrinsicHeight(
                                    child: Container(
                                      constraints: BoxConstraints.expand(),
                                      child: Wrap(
                                        direction: Axis.horizontal,
                                        alignment: WrapAlignment.start,
                                        crossAxisAlignment:
                                            WrapCrossAlignment.center,
                                        spacing: 4,
                                        runSpacing: 4,
                                        children: yourAnswerRow,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: IntrinsicHeight(
                                    child: Container(
                                      constraints: BoxConstraints.expand(),
                                      child: Wrap(
                                        direction: Axis.horizontal,
                                        alignment: WrapAlignment.start,
                                        crossAxisAlignment:
                                            WrapCrossAlignment.center,
                                        spacing: 4,
                                        runSpacing: 4,
                                        children: correctAnswerRow,
                                      ),
                                    ),
                                  ),
                                ),
                                ButtonBar(
                                  children: [
                                    TextButton(
                                        onPressed: () async {
                                          setState(() {
                                            _exerciseLoading = int.parse(
                                                currentAnswer['exercise']
                                                    .toString());
                                          });
                                          globals.api
                                              .call('fetchExercises',
                                                  options: {
                                                    'exercise': currentAnswer[
                                                        'exercise']
                                                  },
                                                  context: context)
                                              .then((response) {
                                            setState(() {
                                              _exerciseLoading = -1;
                                            });
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (c) =>
                                                        ExerciseSolution(
                                                            exercise: response[
                                                                    'response']
                                                                [0])));
                                          });
                                        },
                                        child: _exerciseLoading ==
                                                int.parse(
                                                    currentAnswer['exercise']
                                                        .toString())
                                            ? CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                        Theme.of(context)
                                                            .backgroundColor),
                                              )
                                            : Text(
                                                S.of(context).viewFullExercise))
                                  ],
                                )
                              ],
                            ));
                          }),
                    ),
                  )
                : Column(
                    children: <Widget>[
                      TestAppCard(
                        children: [
                          Text(
                            S.of(context).noDataAvailable,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          ListTile(
                              leading: FaIcon(FontAwesomeIcons.infoCircle),
                              title: Text(S
                                  .of(context)
                                  .errorFetchingYourScoreMaybeTheTestSumbissionWasBroken),
                              trailing: OutlinedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (c) => UserScore()));
                                },
                                child: Text(S.of(context).goBack),
                              ))
                        ],
                      ),
                    ],
                  )
            : CenterProgress()));
  }

  @override
  void initState() {
    Map<String, dynamic> options = Map();
    options['id'] = widget.testId;
    globals.api
        .call("testScore", options: options, context: context)
        .then((response) {
      if (response['response'] is Map) {
        _testScore = response['response'];
      }
      setState(() {
        _testFetched = true;
      });
    });
    super.initState();
  }
}
